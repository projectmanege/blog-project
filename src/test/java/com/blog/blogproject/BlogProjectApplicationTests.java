package com.blog.blogproject;

import com.blog.blogproject.Repository.BlogRepository;
import com.blog.blogproject.Repository.UserBlogRepository;
import com.blog.blogproject.Repository.UserRepository;
import com.blog.blogproject.bean.Blogs;
import com.blog.blogproject.bean.User;
import com.blog.blogproject.bean.UserBlog;
import com.blog.blogproject.mapper.BlogMapper;
import com.blog.blogproject.mapper.UserMapper;
import com.blog.blogproject.service.BlogService;

import com.blog.blogproject.service.impl.ScheduledService;
import com.blog.blogproject.service.impl.UpdateService;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.List;

@SpringBootTest
class BlogProjectApplicationTests {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    BlogMapper blogMapper;

    @Autowired
    BlogRepository blogRepository;

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserBlogRepository userBlogRepository;

    @Autowired
    UpdateService up;

    @Test
    public void save() {
        List<UserBlog> blogs = blogMapper.selectAll();
        for (UserBlog blog : blogs) {
            userBlogRepository.save(blog);
        }
//        BulkRequest bulkRequest = new BulkRequest();
//        bulkRequest.add(
//                new DeleteRequest("qs", "57")
//        );
    }

    @Test
    public void saveMongo() {
        List<User> users = userMapper.findAll();
        for (User user : users) {
            userRepository.save(user);
        }
    }

    /**
     * 清除es
     */
    @Test
    public void clearEs(){
        up.clearEs();
    }


}
