package com.blog.blogproject.component;

import com.blog.blogproject.service.UserService;
import com.blog.blogproject.utils.ContextHolderUtils;
import com.blog.blogproject.utils.TransformBirthday;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Component
public class RememberUserDetailService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserService userService;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        HttpServletRequest request = ContextHolderUtils.getRequest();

        HttpSession session = request.getSession();

        String year, month, day;
        com.blog.blogproject.bean.User user;

        String yzm = request.getParameter("yzm");

        BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();

        String password = request.getParameter("password");

        //角色
        Collection<GrantedAuthority> authorities = authorities();

        user = userService.findById(Integer.valueOf(username));
        String pwd = passwordEncoder.encode(user.getPassword());
        TransformBirthday transformBirthday = new TransformBirthday(user.getBirthday());
        year = transformBirthday.getYear();
        month = transformBirthday.getMonth();
        day = transformBirthday.getDay();
        session.setAttribute("loginUserId", user.getUser_id());
        session.setAttribute("loginUser", user);
        session.setAttribute("year", year);
        session.setAttribute("month", month);
        session.setAttribute("day", day);
        return new User(username, pwd, authorities);
    }

    private Collection<GrantedAuthority> authorities() {
        List<GrantedAuthority> authorityList = new ArrayList<GrantedAuthority>();
        authorityList.add(new SimpleGrantedAuthority("USER"));
        return authorityList;
    }
}
