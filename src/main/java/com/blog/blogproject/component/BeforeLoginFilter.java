package com.blog.blogproject.component;

import com.blog.blogproject.service.UserService;
import com.blog.blogproject.utils.ContextHolderUtils;
import com.blog.blogproject.utils.TransformBirthday;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collection;

public class BeforeLoginFilter extends GenericFilterBean {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserService userService;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = ContextHolderUtils.getRequest();

        HttpSession session = request.getSession();

        String year, month, day;
        com.blog.blogproject.bean.User user;

        String yzm = request.getParameter("yzm");

        BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();

        String password = request.getParameter("password");

        String username = request.getParameter("user_id");

        if (session.getAttribute("yzm") != null) {
            if (session.getAttribute("yzm").equals(yzm)) {
                if (userService.userNumber(Integer.valueOf(username)) != 0) {
                    user = userService.findById(Integer.valueOf(username));
                    String pwd = passwordEncoder.encode(user.getPassword());
                    TransformBirthday transformBirthday = new TransformBirthday(user.getBirthday());
                    year = transformBirthday.getYear();
                    month = transformBirthday.getMonth();
                    day = transformBirthday.getDay();
                    if (!bcryptPasswordEncoder.matches(password, pwd)) {
                        throw new UsernameNotFoundException("用户名或密码错误");
                    } else {
                        session.setAttribute("loginUserId", user.getUser_id());
                        session.setAttribute("loginUser", user);
                        session.setAttribute("year", year);
                        session.setAttribute("month", month);
                        session.setAttribute("day", day);
                        filterChain.doFilter(servletRequest, servletResponse);
                    }
                } else {
                    throw new RuntimeException("用户名不存在");
                }
            } else {
                throw new RuntimeException("验证码错误");
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }
}
