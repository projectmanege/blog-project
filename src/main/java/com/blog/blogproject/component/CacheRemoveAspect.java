package com.blog.blogproject.component;

import com.blog.blogproject.utils.CacheRemove;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.Set;

@Aspect
@Component
public class CacheRemoveAspect {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @AfterReturning("@annotation(com.blog.blogproject.utils.CacheRemove)")  //AfterReturning 增强处理将在目标方法正常完成后被织入
    public void remove(JoinPoint point) {
        Method method = ((MethodSignature) point.getSignature()).getMethod();   //获取目标对象的方法对象
        CacheRemove cacheRemove = method.getAnnotation(CacheRemove.class);
        String[] keys = cacheRemove.value();
        for (String key : keys) {
            if (key.contains("#")) {
                key = parseKey(key, method, point.getArgs());
            }

            Set<String> deleteKeys = stringRedisTemplate.keys(key);
            stringRedisTemplate.delete(deleteKeys);
            logger.trace("cache key: " + key + " deleted");
        }
    }

    /**
     * parseKey from SPEL
     * LocalVariableTableParameterNameDiscoverer获取方法的参数名
     * 利用SPEL的ExpressionParser来解析key，并将有*的部分的值全部获取并存入StandardEvaluationContext中。
     */
    private String parseKey(String key, Method method, Object[] args) {
        LocalVariableTableParameterNameDiscoverer u =
                new LocalVariableTableParameterNameDiscoverer();    //获取方法的参数名
        String[] paraNameArr = u.getParameterNames(method);

        ExpressionParser parser = new SpelExpressionParser();
        StandardEvaluationContext context = new StandardEvaluationContext();    //StandardEvaluationContext处理spel的变量

        for (int i = 0; i < paraNameArr.length; i++) {
            context.setVariable(paraNameArr[i], args[i]);   //获取SPEL解析后满足条件的参数
        }
        return parser.parseExpression(key).getValue(context, String.class);     //ExpressionParser(表达式解析器)
    }
}
