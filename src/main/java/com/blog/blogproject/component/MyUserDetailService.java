package com.blog.blogproject.component;

import com.blog.blogproject.service.UserService;
import com.blog.blogproject.utils.ContextHolderUtils;
import com.blog.blogproject.utils.TransformBirthday;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        HttpServletRequest request = ContextHolderUtils.getRequest();

        HttpSession session = request.getSession();

        String year, month, day;
        com.blog.blogproject.bean.User user = null;
        String pwd = "";

        String yzm = request.getParameter("yzm");

        BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();

        String password = request.getParameter("password");

        //角色
        Collection<GrantedAuthority> authorities = authorities();

        if (session.getAttribute("yzm") != null) {
            if (session.getAttribute("yzm").equals(yzm)) {
                if (StringUtils.isNumeric(username)) {
                    if (userService.userNumber(Integer.valueOf(username)) == 1) {
                        user = userService.findById(Integer.valueOf(username));
                        pwd = user.getPassword();
                        TransformBirthday transformBirthday = new TransformBirthday(user.getBirthday());
                        year = transformBirthday.getYear();
                        month = transformBirthday.getMonth();
                        day = transformBirthday.getDay();
                        if (!bcryptPasswordEncoder.matches(password, pwd)) {
                            throw new UsernameNotFoundException("用户名或密码错误");
                        } else {
                            session.setAttribute("loginUserId", user.getUser_id());
                            session.setAttribute("loginUser", user);
                            session.setAttribute("year", year);
                            session.setAttribute("month", month);
                            session.setAttribute("day", day);
                            return new User(username, pwd, authorities);
                        }
                    } else {
                        throw new RuntimeException("用户名不存在");
                    }
                } else {
                    if (userService.emailNumber(username) == 1) {
                        user = userService.findByemail(username);
                        pwd = user.getPassword();
                        TransformBirthday transformBirthday = new TransformBirthday(user.getBirthday());
                        year = transformBirthday.getYear();
                        month = transformBirthday.getMonth();
                        day = transformBirthday.getDay();
                        if (!bcryptPasswordEncoder.matches(password, pwd)) {
                            throw new UsernameNotFoundException("用户名或密码错误");
                        } else {
                            session.setAttribute("loginUserId", user.getUser_id());
                            session.setAttribute("loginUser", user);
                            session.setAttribute("year", year);
                            session.setAttribute("month", month);
                            session.setAttribute("day", day);
                            return new User(username, pwd, authorities);
                        }
                    } else {
                        throw new RuntimeException("用户名不存在");
                    }
                }
            } else {
                throw new RuntimeException("验证码错误");
            }
        } else {
            if (StringUtils.isNumeric(username)) {
                if (userService.userNumber(Integer.valueOf(username)) == 1) {
                    user = userService.findById(Integer.valueOf(username));
                    pwd = user.getPassword();
                    TransformBirthday transformBirthday = new TransformBirthday(user.getBirthday());
                    year = transformBirthday.getYear();
                    month = transformBirthday.getMonth();
                    day = transformBirthday.getDay();
                    session.setAttribute("loginUserId", user.getUser_id());
                    session.setAttribute("loginUser", user);
                    session.setAttribute("year", year);
                    session.setAttribute("month", month);
                    session.setAttribute("day", day);
                }
                return new User(username, pwd, true, true, true, true, authorities);

            } else {
                if (userService.emailNumber(username) == 1) {
                    user = userService.findByemail(username);
                    pwd = user.getPassword();
                    TransformBirthday transformBirthday = new TransformBirthday(user.getBirthday());
                    year = transformBirthday.getYear();
                    month = transformBirthday.getMonth();
                    day = transformBirthday.getDay();
                    session.setAttribute("loginUserId", user.getUser_id());
                    session.setAttribute("loginUser", user);
                    session.setAttribute("year", year);
                    session.setAttribute("month", month);
                    session.setAttribute("day", day);
                }
                return new User(username, pwd, true, true, true, true, authorities);

            }
        }
    }


    private Collection<GrantedAuthority> authorities() {
        List<GrantedAuthority> authorityList = new ArrayList<GrantedAuthority>();
        authorityList.add(new SimpleGrantedAuthority("USER"));
        return authorityList;
    }
}
