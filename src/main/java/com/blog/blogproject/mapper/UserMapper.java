package com.blog.blogproject.mapper;

import com.blog.blogproject.bean.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
@Repository
public interface UserMapper {

    /**
     * 查询所有
     *
     * @return
     */
    @Select("select * from user")
    List<User> findAll();

    /**
     * 根据id查询用户
     *
     * @return
     */
    @Select("select * from user where user_id = ${user_id}")
    User findById(Integer user_id);

    /**
     * 根据账户名查找该账户名数量
     *
     * @param user_id
     * @return
     */
    @Select("Select count(*) from user where user_id = ${user_id} ")
    Integer userNumber(Integer user_id);

    /**
     * 根据邮箱查询账户
     *
     * @param email
     * @return
     */
    @Select("select * from user where email = #{email}")
    User findByemail(String email);

    /**
     * 查找邮箱是否已经注册
     *
     * @param email
     * @return
     */
    @Select("Select count(*) from user where email = #{email} ")
    Integer emailNumber(String email);

    /**
     * 新增用户
     *
     * @param user
     */
    @Insert("insert into user (user_id,user_name,password,avatar,user_sex,birthday,phone_number,email)values('${user_id}','${user_name}','${password}','${avatar}','${user_sex}','${birthday}','${phone_number}','${email}') ")
    void insertUser(User user);

    /**
     * 新增没有填写生日的用户
     *
     * @param user
     */
    @Insert("insert into user (user_id,user_name,password,avatar,user_sex,phone_number,email)values('${user_id}','${user_name}','${password}','${avatar}','${user_sex}','${phone_number}','${email}') ")
    void insertUserWithoutBirthday(User user);

    /**
     * 根据id删除账户
     *
     * @param id
     */
    @Update("delete from user where id = ${id}")
    void deleteUserById(Integer id);

    /**
     * 根据用户账号和用户名查询用户
     *
     * @param userId
     * @return
     */
    @Select("select * from user where user_id = #{userId} or user_name = #{userId}")
    List<User> findUserByUesrIdOrName(String userId);

    /**
     * 更新用户
     *
     * @param user
     */
    @Update("update user set user_id=#{user_id},user_name=#{user_name},avatar=#{avatar},user_sex=#{user_sex},birthday=#{birthday},phone_number=#{phone_number},email=#{email} where id=#{id} ")
    void updateUser(User user);

    /**
     * 修改密码
     */
    @Update("update user set password=#{password} where user_id=#{user_id} ")
    void updatePwd(Integer user_id, String password);
}
