package com.blog.blogproject.mapper;

import com.blog.blogproject.bean.Reply;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ReplyMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Reply record);

    Reply selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(Reply record);

    List<Reply> findReplyByCommentId(Integer commentId);

    String findAvatarByReplyId(Integer replyId);

    String selectUserNameByReplyId(Integer replyId);

}