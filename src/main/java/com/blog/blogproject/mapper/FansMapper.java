package com.blog.blogproject.mapper;

import com.blog.blogproject.bean.FanedInf;
import com.blog.blogproject.bean.Fans;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface FansMapper {
    /**
     * 查找所有粉丝
     *
     * @return
     */
    @Select("SELECT * FROM user where user_id=any(SELECT fans_id FROM fans WHERE user_id=#{user_id})")
    List<FanedInf> ShowAllFans(Integer user_id);

    /**
     * 查找我关注的人
     */
    @Select("SELECT * FROM user where user_id=any(SELECT user_id FROM fans WHERE fans_id=#{user_id})")
    List<FanedInf> findById(Integer user_id);

    /**
     * 关注用户
     */
    @Insert("INSERT INTO fans(`user_id`, `fans_id`) VALUES (#{fans_id}, #{user_id})")
    void insertFans(Fans fans);

    /**
     * 取关用户 / 移除粉丝
     */
    @Delete("DELETE FROM fans where user_id=#{user_id} and fans_id=#{fans_id}")
    void deleteFans(Integer user_id, Integer fans_id);

    /**
     * 多少人关注我
     *
     * @param user_id
     * @return
     */
    @Select("SELECT COUNT(*) FROM fans WHERE user_id=#{user_id}")
    int countFans(Integer user_id);

    /**
     * 我关注了多少人
     *
     * @param user_id
     * @return
     */
    @Select("SELECT COUNT(*) FROM fans WHERE fans_id=#{user_id}")
    int countFaned(Integer user_id);

    @Select("SELECT COUNT(*) FROM fans WHERE user_id=#{currentUserId} and fans_id=#{user_id}")
    int count(Integer currentUserId, Integer user_id);
}