package com.blog.blogproject.mapper;

import com.blog.blogproject.bean.Comment;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface CommentMapper {

    List<Comment> findCommentByUserAndBlog(Integer blogId);

    void deleteByPrimaryKey(Integer id);

    void insert(Comment record);

    int insertSelective(Comment record);

    Comment selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Comment record);

    int updateByPrimaryKey(Comment record);

    String selectAvatarByCommentId(Integer commentId);

    void addNice(Integer commentId);

    String selectUserNameByCommentId(Integer commentId);
}