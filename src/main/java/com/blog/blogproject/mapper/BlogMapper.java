package com.blog.blogproject.mapper;

import com.blog.blogproject.bean.Blogs;
import com.blog.blogproject.bean.User;
import com.blog.blogproject.bean.UserBlog;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface BlogMapper {
    //查询用户全部博客
    @Select("select distinct a.id,a.user_id,types,title,text,a.create_time,number_of_readers,likes,imgs, avatar,user_sex,birthday,phone_number,email,b.user_name from blogs a, user b where a.user_id=b.user_id and a.user_id=#{user_id} order by a.id desc")
    List<UserBlog> selectAllBlogs(Integer user_id);

    //查询全部博客
    @Select("select distinct a.id,a.user_id,types,title,text,a.create_time,number_of_readers,likes,imgs, avatar,user_sex,birthday,phone_number,email,b.user_name from blogs a, user b where a.user_id=b.user_id order by a.id desc")
    List<UserBlog> selectAll();

    @Update("update blogs set likes=likes+1 where id=#{id}")
    void likes(Integer id);

    //根据title或用户名或用户id搜索博客
    @Select("select distinct a.id,a.user_id,types,title,text,a.create_time,number_of_readers,likes,imgs, avatar,user_sex,birthday,phone_number,email,b.user_name from blogs a, user b where a.user_id=b.user_id and (a.title like '%${search}%' or b.user_id like '%${search}%' or b.user_name like '%${search}%') order by a.id desc")
    List<UserBlog> selectBySearch(String search);

    //根据title或用户名或用户id搜索用户
    @Select("select * from user where user_id like '%${search}%' or user_name like '%${search}%' order by id desc")
    List<User> selectUserBySearch(String search);

    //搜索结果查询
    @Select("select count(*) from blogs a, user b where a.user_id=b.user_id and (a.title like '%${search}%' or text like '%${search}%' or b.user_name like '%${search}%') order by a.id desc")
    int searchCount(String search);

    //搜索用户结果查询
    @Select("select count(*) from user where user_id like '%${search}%' or user_name like '%${search}%' order by id desc")
    int searchUserCount(String search);

    //增加一篇博客
    @Insert("insert into blogs (user_id,types,title,text,number_of_readers,likes,imgs,isSelected)values('${user_id}','${types}','${title}','${text}','${number_of_readers}','${likes}','${imgs}','${isSelected}')")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void addBlog(Blogs blogs);

    //修改一篇博客
    @Update("update blogs set title=#{title},text=#{text},types=#{types} where id = #{id}")
    void updateBlog(Blogs blogs);

    //删除一篇博客
    @Delete("delete from blogs where id = #{id}")
    int delectBlog(@Param("id") int id);

    //我的博客计数统计
    @Select("select count(*) from blogs where user_id=#{user_id}")
    int blogsCount(Integer user_id);

    /**
     * 博客分类计数
     *
     * @param types
     * @return
     */
    @Select("select count(*) from blogs where types = #{types}")
    int blogsTyepCount(String types);

    /**
     * 我的博客分类计数
     *
     * @param types
     * @return
     */
    @Select("select count(*) from blogs where types = #{types} and user_id=#{user_id}")
    int mybBlogsTyepCount(String types, Integer user_id);

    //通过id查找博客
    @Select("select distinct a.id,a.user_id,types,title,text,a.create_time,number_of_readers,likes,imgs, avatar,user_sex,birthday,phone_number,email,b.user_name from blogs a,user b where a.user_id=b.user_id and a.id = #{id}")
    UserBlog selectBlogById(Integer id);

    //查询精选文章
    @Select("select distinct a.id,a.user_id,types,title,text,a.create_time,number_of_readers,likes,imgs, avatar,user_sex,birthday,phone_number,email,b.user_name from blogs a, user b where a.user_id=b.user_id and a.isSelected=1 order by a.id desc")
    List<UserBlog> selectByIsSelected(Integer isSelected);

    //精选文章计数统计
    @Select("select count(*) from blogs where isSelected = 1")
    int isSelectedCount();

    //增加阅读量
    @Update("update blogs set number_of_readers = number_of_readers+1 where id = #{id}")
    void readAdd(Integer blogId);

    /**
     * 根据分类查询博客
     *
     * @param types
     * @return
     */
    @Select("select distinct a.id,a.user_id,types,title,text,a.create_time,number_of_readers,likes,imgs, avatar,user_sex,birthday,phone_number,email,b.user_name from blogs a,user b where a.user_id=b.user_id and types = #{types}")
    List<UserBlog> findByUserBlog(String types);

    /**
     * 根据分类查询我的博客
     *
     * @param types
     * @return
     */
    @Select("select distinct a.id,a.user_id,types,title,text,a.create_time,number_of_readers,likes,imgs, avatar,user_sex,birthday,phone_number,email,b.user_name from blogs a,user b where a.user_id=b.user_id and types = #{types} and a.user_id=#{user_id}")
    List<UserBlog> findByMyBlog(String types, Integer user_id);

}
