package com.blog.blogproject.Repository;

import com.blog.blogproject.bean.Blogs;
import com.blog.blogproject.bean.User;
import com.blog.blogproject.bean.UserMongoDB;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * UserRepository
 *
 * @author qs
 * @date 2021/1/11 0011
 **/
@Repository
public class UserRepository {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 创建对象
     */
    public void save(User user) {
        UserMongoDB userMongoDB = new UserMongoDB(user.getId(),String.valueOf(user.getUser_id()),user.getUser_name(),user.getPassword(),user.getAvatar(),user.getUser_sex(),user.getCreate_time(),user.getBirthday(),user.getPhone_number(),user.getEmail());
        mongoTemplate.save(userMongoDB);
        logger.info("创建对象MongoDB");
    }


    /**
     * 根据用户名查询对象
     * @return
     */
    public User findByUserId(String user_id) {
        Query query=new Query(Criteria.where("userId").is(user_id));
        UserMongoDB userMongoDB =  mongoTemplate.findOne(query, UserMongoDB.class, "user");
        User user = new User();
        user.setId(userMongoDB.getId());
        user.setUser_id(Integer.valueOf(userMongoDB.getUserId()));
        user.setUser_name(userMongoDB.getUser_name());
        user.setPassword(userMongoDB.getPassword());
        user.setUser_sex(userMongoDB.getUser_sex());
        user.setAvatar(userMongoDB.getAvatar());
        user.setCreate_time(userMongoDB.getCreate_time());
        user.setBirthday(userMongoDB.getBirthday());
        user.setPhone_number(userMongoDB.getPhone_number());
        user.setEmail(userMongoDB.getEmail());
        logger.info("查询对象MongoDB");
        return user;
    }

    /**
     * 更新对象
     */
//    public void updateTest(User user) {
//        Query query=new Query(Criteria.where("id").is(user.getId()));
//        Update update= new Update().set("content", user.getContent()).set("date", user.getDate());
//        //更新查询返回结果集的第一条
//        mongoTemplate.updateFirst(query,update,Order.class);
//        //更新查询返回结果集的所有
//        // mongoTemplate.updateMulti(query,update,TestEntity.class);
//    }

    /**
     * 删除对象
     * @param id
     */
    public void deleteTestById(Integer id) {
        Query query=new Query(Criteria.where("user_id").is(id));
        mongoTemplate.remove(query,User.class);
    }

    public List<User> findAll() {
        logger.info("查询所有MongoDB");
        return mongoTemplate.findAll(User.class,"user");
    }

}
