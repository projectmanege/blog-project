package com.blog.blogproject.Repository;

import com.blog.blogproject.bean.User;
import com.blog.blogproject.bean.UserBlog;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;


public interface UserBlogRepository extends ElasticsearchRepository<UserBlog, Integer> {
}
