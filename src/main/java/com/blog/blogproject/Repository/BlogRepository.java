package com.blog.blogproject.Repository;

import com.blog.blogproject.bean.Blogs;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * BlogRepository
 *
 * @author qs
 * @date 2021/1/9 0009
 **/
@Repository
public interface BlogRepository extends MongoRepository<Blogs, String> {
}
