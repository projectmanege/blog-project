package com.blog.blogproject;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

//要使用mapper包内的文件必须要加上@MapperScan
//@MapperScan(basePackages = {"com.blog.blogproject.**.*mapper"})
//@MapperScan("com.blog.blogproject.mapper")
@MapperScan(value = "com.blog.blogproject.mapper")
@EnableRabbit   //开启基于注解的RabbitMQ模式
@EnableAspectJAutoProxy
@SpringBootApplication
@EnableCaching  //开启基于注解的缓存
@EnableScheduling   //开启基于注解的定时任务
public class BlogProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlogProjectApplication.class, args);
    }
    /**
     * mybatis-plus 分页插件 根据我们的业务需求添加
     */
//    @Bean
//    public PaginationInterceptor paginationInterceptor(){
//        return new PaginationInterceptor();
//    }

}
