package com.blog.blogproject.controller;

import com.blog.blogproject.bean.User;
import com.blog.blogproject.service.impl.CommentService;
import com.blog.blogproject.service.impl.ReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.relational.core.sql.In;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class CommentController {

    @Autowired
    CommentService commentService;

    @Autowired
    ReplyService replyService;

    /**
     * 发布评论存入数据库
     */
    @RequestMapping(value = "/commentAdd", method = RequestMethod.POST)
    public String commentAdd(HttpServletRequest request, String comment, Integer blogId) {
        Integer user_id = (Integer) request.getSession().getAttribute("loginUserId");
        commentService.commentAdd(user_id, comment, blogId);
        return "redirect:/readBlog/?blogId=" + blogId;
    }

    /**
     * 点赞
     *
     * @return
     */
    @RequestMapping(value = "/nice")
    public String nice(Integer commentId, Integer blogId) {
        commentService.addNice(commentId);
        return "redirect:/readBlog/?blogId=" + blogId;
    }

    /**
     * 删除评论
     *
     * @param commentId
     * @return
     */
    @RequestMapping("/delComment")
    public String delComment(Integer commentId, Integer blogId) {
        commentService.delComment(commentId);
        return "redirect:/readBlog/?blogId=" + blogId;
    }

    /**
     * 评论回复添加到数据库
     *
     * @param replyText
     * @param userID
     * @param userName
     * @param commentId
     * @return
     */
    @RequestMapping("/replyAdd")
    public String reply(HttpServletRequest request, String replyText, Integer userID, String userName, Integer commentId, Integer blogId) {
        Integer user_id = (Integer) request.getSession().getAttribute("loginUserId");
        User user = (User) request.getSession().getAttribute("loginUser");
        String user_name = user.getUser_name();
        System.out.println("replyText:" + replyText + "|userID:" + userID + "|userName:" + userName + "|commentId:" + commentId);
        replyService.replyAdd(replyText, user_id, user_name, commentId);
        return "redirect:/readBlog/?blogId=" + blogId;
    }

}
