package com.blog.blogproject.controller;

import com.blog.blogproject.Repository.UserRepository;
import com.blog.blogproject.bean.User;
import com.blog.blogproject.mapper.UserMapper;
import com.blog.blogproject.service.UserService;
import com.blog.blogproject.utils.EmailCodeUtils;
import com.blog.blogproject.utils.TransformBirthday;
import com.github.pagehelper.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping("/user")
public class UserController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    UserService userService;

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserRepository userRepository;

    /**
     * 查找所有
     *
     * @return
     */
    @RequestMapping("/findAll")
    public String findAll(Map<String, Object> map) {
        List<User> users;
        users = userService.findAll();
        map.put("users", users);
        return "findAll";
    }

    /**
     * 根据id查询用户
     *
     * @return
     */
    @RequestMapping("/findById")
    public String findById(Integer user_id, Model model) {
//        User user = userService.findById(user_id);
        User user = userRepository.findByUserId(String.valueOf(user_id));
        model.addAttribute("user", user);
        return "findById";
    }

    @RequestMapping("/test")
    public String test() {
        return "test";
    }

    /**
     * 发送邮箱验证码
     *
     * @param email
     * @return
     */
    @ResponseBody
    @RequestMapping("/sendEmailCode")
    public String sendMail(String email) {
        if (userService.emailNumber(email) == 0) {
            rabbitTemplate.convertAndSend("qs.blog", "qs.addUser", email);
            return "success";
        } else {
            return "fail";
        }
    }

    /**
     * 修改密码发送验证短信
     *
     * @param email
     * @return
     */
    @ResponseBody
    @RequestMapping("/sendUpdateEmail")
    public String sendUpdateMail(String email) {
        rabbitTemplate.convertAndSend("qs.blog", "qs.addUser", email);
        return "success";
    }

    /**
     * 忘记密码-修改密码
     *
     * @return
     */
    @RequestMapping("/updatePwd")
    public String updatePassword(String password, String email, Map<String, Object> map) {
        Integer user_id = userService.findByemail(email).getUser_id();
        userMapper.updatePwd(user_id, passwordEncoder.encode(password));
        map.put("msg", "密码修改成功，请登录");
        return "login";
    }

    /**
     * 从个人信息修改密码
     *
     * @param password
     * @return
     */
    @RequestMapping("/updateInformationPwd")
    public String updateInformationPwd(String password, HttpServletRequest request) {
        Integer user_id = (Integer) request.getSession().getAttribute("loginUserId");
        userMapper.updatePwd(user_id, passwordEncoder.encode(password));
        return "redirect:/information";
    }

    /**
     * 验证邮箱
     *
     * @param code
     * @param email
     * @param response
     * @param model
     * @return
     */
    @RequestMapping("/checkEmail")
    public String checkEmail(String code, String email, HttpServletResponse response, Model model) {
        response.setContentType("text/html;charset=utf-8");
        response.setCharacterEncoding("utf-8");
        PrintWriter out = null;
        if (code.equals(redisTemplate.opsForValue().get(email))) {
            model.addAttribute("email", email);
        } else {
            try {
                out = response.getWriter();
            } catch (IOException e) {
                e.printStackTrace();
            }
            out.println("<script language=\"javascript\" type=\"text/javascript\" >");
            out.println("alert('验证码不正确!!!');");
            out.println("history.back();");
            out.println("</script>");
            out.close();
        }
        return "user/updatePwd";
    }

    /**
     * 新增用户
     *
     * @param file
     * @param year
     * @param month
     * @param day
     * @return
     * @throws ParseException
     */
    @RequestMapping("/insertUser")
    public String insertUser(MultipartFile file, User user, String year, String month, String day, String code, Map<String, Object> map, HttpServletResponse response) {
        response.setContentType("text/html;charset=utf-8");
        response.setCharacterEncoding("utf-8");
        PrintWriter out = null;
        boolean flag = true;

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        if (!year.isEmpty() && !month.isEmpty() && !day.isEmpty()) {
            flag = theTrueDate(year, month, day, response);
        }
        if ((year.isEmpty() || month.isEmpty() || day.isEmpty()) && (!year.isEmpty() || !month.isEmpty() || !day.isEmpty())) {
            try {
                out = response.getWriter();
            } catch (IOException e) {
                e.printStackTrace();
            }

            flag = false;
            out.println("<script language=\"javascript\" type=\"text/javascript\" >");
            out.println("alert('请填写完整的生日字段!!!');");
            out.println("history.back();");
            out.println("</script>");
            out.close();
        }
        if (user.getUser_id() < 100000) {
            try {
                out = response.getWriter();
            } catch (IOException e) {
                e.printStackTrace();
            }
            flag = false;
            out.println("<script language=\"javascript\" type=\"text/javascript\" >");
            out.println("alert('注册账号只能六位或以上数字!!!');");
            out.println("history.back();");
            out.println("</script>");
            out.close();
        }
        if (StringUtil.isNotEmpty(user.getPhone_number())) {
            String phonenum = user.getPhone_number();
            boolean isNum = phonenum.matches("[0-9]+");
            boolean isEleven = phonenum.matches("\\d{11}");
            if (!isEleven || !isNum) {
                try {
                    out = response.getWriter();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                flag = false;
                out.println("<script language=\"javascript\" type=\"text/javascript\" >");
                out.println("alert('手机号码只能为11位纯数字!!!');");
                out.println("history.back();");
                out.println("</script>");
                out.close();
            }
        }
        if (userService.userNumber(user.getUser_id()) == 0) {   //判断账号是否存在
//            if (code.equals(redisTemplate.opsForValue().get(user.getEmail()))) {
                if (flag) {
                    if (!year.isEmpty() && !month.isEmpty() && !day.isEmpty()) {
                        userService.insertUser(file, user, year, month, day);
                    } else {
                        userService.insertUserWithoutBirthday(file, user, year, month, day);
                    }
                map.put("msg", "注册成功，请登录");
            } else {
                    try {
                        out = response.getWriter();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    out.println("<script language=\"javascript\" type=\"text/javascript\" >");
                    out.println("alert('验证码不正确!!!');");
                    out.println("history.back();");
                    out.println("</script>");
                    out.close();
                }
        } else {
            try {
                out = response.getWriter();
            } catch (IOException e) {
                e.printStackTrace();
            }
            out.println("<script language=\"javascript\" type=\"text/javascript\" >");
            out.println("alert('账号已存在!!!');");
            out.println("history.back();");
            out.println("</script>");
            out.close();
        }
        return "login";
    }


    /**
     * 根据id删除用户
     *
     * @param id
     * @return
     */
    @RequestMapping("/deleteUserById")
    public String deleteUserById(Integer id) {
        userService.deleteUserById(id);
        return "deleteUserById";
    }

    /**
     * 根据用户账号和用户名查询用户
     *
     * @param userId
     * @return
     */
    @RequestMapping("/findUserByUserIdOrName")
    public String findUserByUserIdOrName(String userId, Map<String, Object> map) {
        List<User> users;
        users = userService.findUserByUserIdOrName(userId);
        map.put("users", users);
        return "findUserByUserIdOrName";
    }

    /**
     * 更新用户
     *
     * @param file
     * @param user
     * @param year
     * @param month
     * @param day
     * @return
     */
    @RequestMapping("/updateUser")
    public String updateUser(MultipartFile file, User user, String year, String month, String day, HttpServletRequest request, HttpServletResponse response) {
        Integer user_id = (Integer) request.getSession().getAttribute("loginUserId");

        response.setContentType("text/html;charset=utf-8");
        response.setCharacterEncoding("utf-8");
        PrintWriter out = null;
        boolean flag = true;

        if (!year.isEmpty() && !month.isEmpty() && !day.isEmpty()) {
            flag = theTrueDate(year, month, day, response);
        }
        if ((year.isEmpty() || month.isEmpty() || day.isEmpty()) && (!year.isEmpty() || !month.isEmpty() || !day.isEmpty())) {
            try {
                out = response.getWriter();
            } catch (IOException e) {
                e.printStackTrace();
            }
            flag = false;
            out.println("<script language=\"javascript\" type=\"text/javascript\" >");
            out.println("alert('请填写完整的生日字段!!!');");
            out.println("history.back();");
            out.println("</script>");
            out.close();
        }
        if (user.getPhone_number() != null) {
            String phonenum = user.getPhone_number();
            boolean isNum = phonenum.matches("[0-9]+");
            boolean isEleven = phonenum.matches("\\d{11}");
            if (!isEleven || !isNum) {
                try {
                    out = response.getWriter();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                flag = false;
                out.println("<script language=\"javascript\" type=\"text/javascript\" >");
                out.println("alert('手机号码只能为11位纯数字!!!');");
                out.println("history.back();");
                out.println("</script>");
                out.close();
            }
        }
        if (flag == true) {
            userService.updateUser(file, user_id, user, year, month, day);
            return "redirect:/user/skip";
        } else {
            return "user/information";
        }
    }

    //跳转一次来更新session
    @RequestMapping(value = "/skip")
    public String skip(HttpSession session, HttpServletRequest request) {
        String year, month, day;
        Integer userId = (Integer) request.getSession().getAttribute("loginUserId");
        User user = userService.findById(userId);
        TransformBirthday transformBirthday = new TransformBirthday(user.getBirthday());
        year = transformBirthday.getYear();
        month = transformBirthday.getMonth();
        day = transformBirthday.getDay();
        request.getSession().setAttribute("loginUser", user);
        request.getSession().setAttribute("year", year);
        request.getSession().setAttribute("month", month);
        request.getSession().setAttribute("day", day);
        return "redirect:/information";
    }

    /**
     * 判断日期是否输入正确
     *
     * @param year
     * @param month
     * @param day
     * @param response
     */
    public boolean theTrueDate(String year, String month, String day, HttpServletResponse response) {
        PrintWriter out = null;
        boolean flag = false;
        try {
            out = response.getWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (!year.matches("[0-9]+") || !month.matches("[0-9]+") || !day.matches("[0-9]+")) {
            out.println("<script language=\"javascript\" type=\"text/javascript\" >");
            out.println("alert('生日请填写数字');");
            out.println("history.back();");
            out.println("</script>");
            return false;
        }

        int yearNum = Integer.valueOf(year).intValue();
        int monthNum = Integer.valueOf(month).intValue();
        int dayNum = Integer.valueOf(day).intValue();

        if (yearNum < 1900 || yearNum > 2020) {
            out.println("<script language=\"javascript\" type=\"text/javascript\" >");
            out.println("alert('生日区间在1900-2020范围');");
            out.println("history.back();");
            out.println("</script>");
        } else if (monthNum <= 0 || monthNum > 12) {
            out.println("<script language=\"javascript\" type=\"text/javascript\" >");
            out.println("alert('请输入正确的月份');");
            out.println("history.back();");
            out.println("</script>");
        } else if (dayNum <= 0) {
            out.println("<script language=\"javascript\" type=\"text/javascript\" >");
            out.println("alert('请输入正确的日期');");
            out.println("history.back();");
            out.println("</script>");
        } else if (dayNum > 0) {

            if ((monthNum == 1 || monthNum == 3 || monthNum == 5 || monthNum == 7 || monthNum == 8 || monthNum == 10 || monthNum == 12) && (dayNum > 31)) {
                out.println("<script language=\"javascript\" type=\"text/javascript\" >");
                out.println("alert('" + monthNum + "月份的日期应不大于31');");
                out.println("history.back();");
                out.println("</script>");
            } else if ((monthNum == 4 || monthNum == 6 || monthNum == 9 || monthNum == 11) && (dayNum > 30)) {
                out.println("<script language=\"javascript\" type=\"text/javascript\" >");
                out.println("alert('" + monthNum + "月份的日期应不大于30');");
                out.println("history.back();");
                out.println("</script>");
            } else {
                if ((yearNum % 4 == 0 && yearNum % 100 != 0) || yearNum % 400 == 0) {
                    if (monthNum == 2 && dayNum > 29) {
                        out.println("<script language=\"javascript\" type=\"text/javascript\" >");
                        out.println("alert('" + yearNum + "为闰年2月份的日期应不大于29');");
                        out.println("history.back();");
                        out.println("</script>");
                    } else {
                        flag = true;
                    }
                } else {
                    if (monthNum == 2 && dayNum > 28) {
                        out.println("<script language=\"javascript\" type=\"text/javascript\" >");
                        out.println("alert('" + yearNum + "不为闰年2月份的日期应不大于28');");
                        out.println("history.back();");
                        out.println("</script>");
                    } else {
                        flag = true;
                    }
                }
            }

        }

        return flag;

    }

}
