package com.blog.blogproject.controller;

import com.blog.blogproject.bean.User;
import com.blog.blogproject.service.UserService;
import com.blog.blogproject.utils.TransformBirthday;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Random;

@Controller
public class LoginController {

    @Autowired
    UserService userService;

    @PostMapping(value = "/user/login")
    //@RequestMapping(value = "/user/login",method = RequestMethod.POST)
    public String login(@RequestParam("user_id") String user_id,
                        @RequestParam("password") String password,
                        @RequestParam("yzm") String yzm,
                        Map<String, Object> map, HttpSession session) {
        String year, month, day;
        User user;

        if (StringUtils.isNumeric(user_id)) {
            if (userService.userNumber(Integer.valueOf(user_id)) == 1) {
                user = userService.findById(Integer.valueOf(user_id));
            } else {
                map.put("msg", "账号不存在");
                return "login";
            }
        } else {
            if (userService.emailNumber(user_id) == 1) {
                user = userService.findByemail(user_id);
            } else {
                map.put("msg", "账号不存在");
                return "login";
            }
        }

        if (session.getAttribute("yzm").equals(yzm)) {
            if (user != null && user.getPassword().trim().equals(password)) {
                //登录成功，防止表单重复提交，可以重定向到主页
                TransformBirthday transformBirthday = new TransformBirthday(user.getBirthday());
                year = transformBirthday.getYear();
                month = transformBirthday.getMonth();
                day = transformBirthday.getDay();
                session.setAttribute("loginUserId", user.getUser_id());
                session.setAttribute("loginUser", user);
                session.setAttribute("year", year);
                session.setAttribute("month", month);
                session.setAttribute("day", day);
                return "redirect:/loginSkip";
            } else {
                //登录失败
                map.put("msg", "账号或密码错误");
                return "login";
            }
        } else {
            map.put("msg", "验证码错误");
            return "login";
        }
    }

    @RequestMapping("/user/loginOut")
    public String loginOut(HttpSession session) {
        session.invalidate();
        return "login";
    }

    //多跳转一次来更新session
    @RequestMapping("/loginSkip")
    public String loginSkip() {
        return "redirect:/main";
    }

    @RequestMapping("/verification")
    public void verification(Random random, HttpServletRequest request, HttpServletResponse response) {
        //设置不透明
        BufferedImage image = new BufferedImage(90, 30, BufferedImage.TYPE_3BYTE_BGR);
        Graphics graphics = image.getGraphics();//GDI图像绘制
        graphics.setColor(Color.getColor("#17a2b8"));//绘制背景颜色
        graphics.fillRect(0, 0, 90, 30);//设置图像区域
        graphics.setFont(new Font("微软雅黑", Font.ITALIC, 18));//设置字体样式

        for (int i = 1; i < 10; i++) {  //画线
            graphics.setColor(new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256)));
            int x1 = random.nextInt(90);
            int y1 = random.nextInt(30);
            int x2 = random.nextInt(90);
            int y2 = random.nextInt(30);
            graphics.drawLine(x1, y1, x2, y2);//画线，随机两点连线，即是一条干扰线
        }
        String str = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890";
        char[] c = str.toCharArray();
        StringBuffer strb = new StringBuffer();
        for (int i = 0; i < 4; i++) {
            int x = random.nextInt(c.length);
            strb.append(c[x]);
        }

        request.getSession().setAttribute("yzm", strb.toString().toLowerCase());

        for (int i = 0; i < strb.length(); i++) {
            graphics.setColor(new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256)));
            char z = strb.toString().charAt(i);
            graphics.drawString(z + "", 10 + (i * 20), 20);
        }
        graphics.dispose();//释放资源

        try {
            ImageIO.write(image, "png", response.getOutputStream());//图片输出
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
