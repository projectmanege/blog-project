package com.blog.blogproject.controller;

import com.blog.blogproject.Repository.UserBlogRepository;
import com.blog.blogproject.Repository.UserRepository;
import com.blog.blogproject.bean.User;
import com.blog.blogproject.bean.UserBlog;
import com.blog.blogproject.service.BlogService;
import com.blog.blogproject.service.FansService;
import com.blog.blogproject.service.UserService;
import com.blog.blogproject.utils.DelTagsUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class IndexController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    FansService fansService;
    @Autowired
    BlogService blogService;
    @Autowired
    UserService userService;

    @Autowired
    UserBlogRepository repository;

    @Autowired
    ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Autowired
    RestHighLevelClient elasticsearchClient;

    @Autowired
    UserRepository userRepository;

    @RequestMapping("/")
    public String index() {
        return "redirect:/main";
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/blog")
    public String blogDetail() {
        return "blog";
    }

    @RequestMapping("/create")
    public String createAccount() {
        return "user/create";
    }

    @RequestMapping("/forgetPwd")
    public String forgetPassword() {
        return "user/forgetPwd";
    }

    @RequestMapping("/toUpdateInformationPwd")
    public String toUpdateInformationPwd() {
        return "user/informationUpdatePwd";
    }

    //个人信息
    @RequestMapping("/information")
    public String information(Model model, HttpServletRequest request, HttpSession session) {

        Integer userId = (Integer) request.getSession().getAttribute("loginUserId");
//
//        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//
//        String user_id = ((UserDetails)principal).getUsername();
//
//        if (StringUtils.isNumeric(user_id)) {
//            if (userService.userNumber(Integer.valueOf(user_id)) == 1) {
//                User user = userService.findById(Integer.valueOf(user_id));
//            }
//        } else {
//            if (userService.emailNumber(user_id) == 1) {
//                User user = userService.findByemail(user_id);
//            }
//        }
//        Integer userId = Integer.valueOf(((UserDetails)principal).getUsername());

        Integer blogsCount = blogService.blogsCount(userId);
        Integer fansNum = fansService.countFans(userId);
        Integer fanedNum = fansService.countFaned(userId);
        User user = userService.findById(userId);
        session.setAttribute("loginUser", user);
        model.addAttribute("fansNum", fansNum);
        model.addAttribute("fanedNum", fanedNum);
        model.addAttribute("blogsCount", blogsCount);
        return "user/information";
    }

    //跳转新增博客页面
    @RequestMapping("/addBlog")
    public String addBlog() {
        return "blog/addBlog";
    }

    //跳转修改博客页面
    @RequestMapping("/toUpdateBlog")
    public String updateBlog(Integer blogId, Model model) {
        UserBlog blogs = blogService.selectBlogById(blogId);
        String text = blogs.getText();
        String html = HtmlUtils.htmlUnescape(text);
        blogs.setText(html);
        model.addAttribute("blogs", blogs);
        return "blog/updateBlog";
    }

    @GetMapping("/userBlog/{user_id}")
    public String addUserBlog(@PathVariable("user_id") Integer user_id, HttpServletRequest request) {
        request.getSession().setAttribute("userBlog", user_id);
        return "redirect:/showUserBlog";
    }

    @RequestMapping("/showUserBlog")
    public String userBlog(@RequestParam(defaultValue = "1") Integer page, Model model, HttpServletRequest request) {
        Integer currentUserId = (Integer) request.getSession().getAttribute("userBlog");
        Integer user_id = (Integer) request.getSession().getAttribute("loginUserId");
        Integer fanedCount = fansService.count(currentUserId, user_id);
        Integer fansCount = fansService.count(user_id, currentUserId);
        PageHelper.startPage(page, 5);
        List<UserBlog> list = blogService.selectAllBlogs(currentUserId);
//        User user = userService.findById(currentUserId);
        //MongoDB查询用户
        User user = userRepository.findByUserId(String.valueOf(currentUserId));
        String dateStr = "";
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if (user.getBirthday() != null) {
            try {
                dateStr = sdf.format(user.getBirthday());
                user.setStrBirthday(dateStr);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (user.getUser_sex().equals("1")) {
            user.setUser_sex("男");
        } else if (user.getUser_sex().equals("2")) {
            user.setUser_sex("女");
        }
        int count = blogService.blogsCount(currentUserId);
        model.addAttribute("page", page);
        PageInfo<UserBlog> pageInfo = new PageInfo<>(list, 5);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("count", count);
        model.addAttribute("userInfomation", user);
        model.addAttribute("fanedCount", fanedCount);
        model.addAttribute("fansCount", fansCount);
        return "blog/userBlog";
    }

    //存入搜索框数据
    @RequestMapping("/search")
    public String search(@RequestParam String search, HttpServletRequest request) {
        request.getSession().setAttribute("search", search);
        return "redirect:/toSearch";
    }

    //通过搜索框搜索
    @RequestMapping("/toSearch")
    public String toSearch(@RequestParam(defaultValue = "1") Integer page, Model model, HttpServletRequest request) throws IOException {
        if (page < 1) {
            page = 1;
        }
        String search = (String) request.getSession().getAttribute("search");
//        PageHelper.startPage(1, 5);
//        List<UserBlog> list = blogService.findBySearch(search);
////        List<UserBlog> list = repository.findByTitleLike(search);
//        model.addAttribute("page", page);
//        PageInfo<UserBlog> pageInfo = new PageInfo<>(list, 5);
//        model.addAttribute("pageInfo", pageInfo);
//        model.addAttribute("count", count);

//        int count = blogService.searchCount(search);

        ArrayList<Map<String, Object>> list = blogService.findBySearch(search, page);

        int count = blogService.searchCount(search);

        int pages;

        if (count % 5 == 0) {
            pages = count / 5;
        } else {
            pages = count / 5 + 1;
        }

        model.addAttribute("page", page);
        model.addAttribute("pageInfo", list);
        model.addAttribute("count", count);
        model.addAttribute("pages", pages);
        return "blog/searchBlog";
    }

    //通过搜索框搜索用户
    @RequestMapping("/toSearchUser")
    public String toSearchUser(@RequestParam(defaultValue = "1") Integer page, Model model, HttpServletRequest request) {
        page = page == 0 ? 1 : page;
        String search = (String) request.getSession().getAttribute("search");
        PageHelper.startPage(page, 8);
        List<User> list = blogService.findByUserSearch(search);
        String dateStr = "";
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (User user : list) {
            if (user.getBirthday() != null) {
                try {
                    dateStr = sdf.format(user.getBirthday());
                    user.setStrBirthday(dateStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (user.getUser_sex().equals("1")) {
                user.setUser_sex("男");
            } else if (user.getUser_sex().equals("2")) {
                user.setUser_sex("女");
            }
        }
        int count = blogService.searchUserCount(search);
        model.addAttribute("page", page);
        PageInfo<User> pageInfo = new PageInfo<>(list, 8);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("count", count);
        return "blog/searchUser";
    }

    @RequestMapping("/config")
    @ResponseBody
    public String uploadConfig() {
        String s = "{\n" +
                "            \"imageActionName\": \"uploadimage\",\n" +
                "                \"imageFieldName\": \"upfile\", \n" +
                "                \"imageMaxSize\": 2048000, \n" +
                "                \"imageAllowFiles\": [\".png\", \".jpg\", \".jpeg\", \".gif\", \".bmp\"], \n" +
                "                \"imageCompressEnable\": true, \n" +
                "                \"imageCompressBorder\": 1600, \n" +
                "                \"imageInsertAlign\": \"none\", \n" +
                "                \"imageUrlPrefix\": \"\",\n" +
                "                \"imagePathFormat\": \"/asserts/blogImg/{yyyy}{mm}{dd}/{time}{rand:6}\" }";
        return s;
    }

    @RequestMapping("/niceBlog1")
    public String niceBlog1() {
        return "blog/niceBlog1";
    }

    @RequestMapping("/niceBlog2")
    public String niceBlog2() {
        return "blog/niceBlog2";
    }

    @RequestMapping("/announcementBlog")
    public String announcementBlog() {
        return "blog/announcementBlog";
    }

}
