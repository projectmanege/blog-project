package com.blog.blogproject.controller;

import com.blog.blogproject.bean.FanedInf;
import com.blog.blogproject.bean.Fans;
import com.blog.blogproject.service.FansService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/fans")
public class FansController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    FansService fansService;

    /**
     * 查找我的所有粉丝
     */
    @RequestMapping("/findFans")
    public String ShowAllFans(HttpServletRequest request, Model model) {
        Integer user_id = (Integer) request.getSession().getAttribute("loginUserId");
        List<FanedInf> fans = fansService.ShowAllFans(user_id);
        Integer fansNum = fansService.countFans(user_id);
        model.addAttribute("fans", fans);
        model.addAttribute("fansCount", fansNum);
        return "fans/fans";
    }

    /**
     * 查找我关注的人
     *
     * @param model
     * @return
     */
    @RequestMapping("/findFaned")
    public String findById(HttpServletRequest request, Model model) {
        Integer user_id = (Integer) request.getSession().getAttribute("loginUserId");
        List<FanedInf> faneds = fansService.findById(user_id);
        Integer fanedNum = fansService.countFaned(user_id);
        model.addAttribute("faneds", faneds);
        model.addAttribute("fanedCount", fanedNum);
        return "fans/faned";
    }

    /**
     * 关注用户
     *
     * @param currentUserId
     */
    @GetMapping("/addFaned/{currentUserId}")
    public String insertFans(@PathVariable("currentUserId") Integer currentUserId, HttpServletRequest request) {

        Integer user_id = (Integer) request.getSession().getAttribute("loginUserId");
        fansService.insertFans(currentUserId, user_id);
        return "redirect:/userBlog/" + currentUserId;
    }

    /**
     * 取关用户
     *
     * @param fanedId
     */
    @GetMapping("/delFaned/{fanedId}")
    public String deleteFaned(@PathVariable("fanedId") Integer fanedId, HttpServletRequest request) {
        Integer user_id = (Integer) request.getSession().getAttribute("loginUserId");
        fansService.deleteFaned(fanedId, user_id);
        return "redirect:/fans/findFaned";
    }

    /**
     * 用户空间取关
     *
     * @param currentUserId
     */
    @GetMapping("/deleteFanedUser/{currentUserId}")
    public String deleteFanedUser(@PathVariable("currentUserId") Integer currentUserId, HttpServletRequest request) {
        Integer user_id = (Integer) request.getSession().getAttribute("loginUserId");
        fansService.deleteFans(currentUserId, user_id);
        return "redirect:/userBlog/" + currentUserId;
    }


    /**
     * 移除粉丝
     *
     * @param fansId
     */
    @GetMapping("/delFans/{fansId}")
    public String deleteFans(@PathVariable("fansId") Integer fansId, HttpServletRequest request) {
        Integer user_id = (Integer) request.getSession().getAttribute("loginUserId");
        fansService.deleteFans(user_id, fansId);
        return "redirect:/fans/findFans";
    }

    /**
     * 用户空间移除粉丝
     *
     * @param currentUserId
     */
    @GetMapping("/deleteFansUser/{currentUserId}")
    public String deleteFansUser(@PathVariable("currentUserId") Integer currentUserId, HttpServletRequest request) {
        Integer user_id = (Integer) request.getSession().getAttribute("loginUserId");
        fansService.deleteFans(user_id, currentUserId);
        return "redirect:/userBlog/" + currentUserId;
    }

    /**
     * 显示粉丝数量
     */
    public Integer countFans(Integer user_id) {
        int fansNum = fansService.countFans(user_id);
        return fansNum;
    }

    /**
     * 显示我关注的数量
     */
    public Integer countFaned(Integer user_id) {
        int fanedNum = fansService.countFaned(user_id);
        return fanedNum;
    }


}


