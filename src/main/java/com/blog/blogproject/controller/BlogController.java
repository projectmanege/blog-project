package com.blog.blogproject.controller;


import com.blog.blogproject.bean.*;
import com.blog.blogproject.service.BlogService;
import com.blog.blogproject.service.impl.CommentService;
import com.blog.blogproject.service.impl.ReplyService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Controller
public class BlogController {

    @Autowired
    private BlogService blogService;

    @Autowired
    CommentService commentService;

    @Autowired
    ReplyService replyService;

    @Autowired
    RabbitTemplate rabbitTemplate;


    //我的博客页面
    @RequestMapping("/myBlogs")
    public String myBlogs(@RequestParam(defaultValue = "1") Integer page, Model model, HttpServletRequest request) {
        Integer currentUserId = (Integer) request.getSession().getAttribute("loginUserId");

//        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//
//        Integer currentUserId = Integer.valueOf(((UserDetails)principal).getUsername());

        //查询所有博客并实现分页
        PageInfo<UserBlog> pageInfo = blogService.getPageBlogList(currentUserId, page, 5);

        int count = blogService.blogsCount(currentUserId);
        model.addAttribute("page", page);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("count", count);
        return "myBlog";
    }

    //阅读原文
    @RequestMapping("/readBlog")
    public String readBlog(@RequestParam Integer blogId, Model model) {

        UserBlog blogs = blogService.selectBlogById(blogId);
        if (blogs.getTypes().equals("1")) {
            blogs.setTypes("科技类型");
        } else if (blogs.getTypes().equals("2")) {
            blogs.setTypes("情感类型");
        } else if (blogs.getTypes().equals("3")) {
            blogs.setTypes("人生经历");
        } else if (blogs.getTypes().equals("4")) {
            blogs.setTypes("生活小妙招");
        } else if (blogs.getTypes().equals("5")) {
            blogs.setTypes("娱乐文化");
        } else if (blogs.getTypes().equals("6")) {
            blogs.setTypes("其他");
        }
        String html = HtmlUtils.htmlUnescape(blogs.getText());
        blogs.setText(html);
        List<Comment> comments = commentService.findCommentByUserAndBlog(blogId);
        for (Comment comment : comments) {
            List<Reply> replyList = replyService.findReplyByCommentId(comment.getId());
            for (Reply reply : replyList) {
                String replyAvatar = replyService.findAvatarByReplyId(reply.getId());
                String replyUserName = replyService.selectUserNameByReplyId(reply.getId());
                reply.setUserName(replyUserName);
                reply.setReplyAvatar(replyAvatar);
            }
            comment.setReplyList(replyList);
            String avatar = commentService.findAvatar(comment.getId());
            String userName = commentService.selectUserNameByCommentId(comment.getId());
            comment.setAvatar(avatar);
            comment.setUserName(userName);

        }
        model.addAttribute("comments", comments);
        model.addAttribute("blogs", blogs);
        return "blog";
    }

    /**
     * 增加阅读量
     *
     * @return
     */
    @RequestMapping("/readAdd")
    public String readAdd(Integer blogId) {
        blogService.readAdd(blogId);
        return "redirect:/readBlog/?blogId=" + blogId;
    }

    //首页精选博客
    @RequestMapping("/main")
    public String isSelected(@RequestParam(defaultValue = "1") Integer pageIndex, Model model) {
        PageHelper.startPage(pageIndex, 5);
        PageInfo<UserBlog> pageInfo = blogService.selectByIsSelected(1, pageIndex, 5);

        int count = blogService.isSelectedCount();
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("count", count);
        return "index";
    }

    //添加博客
    @RequestMapping("/saveBlog")
    public String saveBlog(HttpServletRequest request, Blogs blogs) {
        Integer user_id = (Integer) request.getSession().getAttribute("loginUserId");
        User user = (User) request.getSession().getAttribute("loginUser");
        blogs.setUser_id(user_id);
        //文章标题
        blogs.setTitle(request.getParameter("title"));
        //文章内容
        blogs.setText(request.getParameter("content"));
        //文章类型
        blogs.setTypes(request.getParameter("types"));
        blogService.addBlog(blogs);

        rabbitTemplate.convertAndSend("qs.blog", "qs.update", "update");

        return "redirect:/myBlogs";
    }

    //点赞博客
    @RequestMapping("/likes/{id}")
    public String likes(@PathVariable("id") Integer id, RedirectAttributes attr) {
        blogService.likes(id);
        attr.addAttribute("blogId", id);
        return "redirect:/readBlog/";
    }

    //接收图片
    @RequestMapping("/saveImg")
    @ResponseBody
    public Map<String, Object> images(MultipartFile upfile, HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> params = new HashMap<String, Object>();
        try {
            String basePath = "/home/tomcat/blogImg/";  //与properties文件中lyz.uploading.url相同，未读取到文件数据时为basePath赋默认值
            String ext = "abc" + upfile.getOriginalFilename();
            String fileName = String.valueOf(System.currentTimeMillis()).concat("_").concat("" + new Random().nextInt(6)).concat(".").concat(ext);
            StringBuilder sb = new StringBuilder();
            //拼接保存路径
            sb.append(basePath).append("/").append(fileName);
            //写到c盘
            File f = new File(sb.toString());
            if (!f.exists()) {
                f.getParentFile().mkdirs();
            }
            OutputStream out = new FileOutputStream(f);
            FileCopyUtils.copy(upfile.getInputStream(), out);


            //虚拟图片服务器
            String visitUrl = "http://47.115.140.130:8080/blogImg/";
            visitUrl = visitUrl.concat(fileName);
            f = new File("/home/tomcat/blogImg/" + fileName);
            out = new FileOutputStream(f);
            FileCopyUtils.copy(upfile.getInputStream(), out);


            params.put("state", "SUCCESS");
            params.put("url", visitUrl);
            params.put("size", upfile.getSize());
            params.put("original", fileName);
            params.put("type", upfile.getContentType());
        } catch (Exception e) {
            e.printStackTrace();
            params.put("state", "ERROR");
        }
        return params;
    }

    //更新博客
    @RequestMapping("/updateBlog")
    public String updateBlog(HttpServletRequest request, Blogs blogs) {

        User user = (User) request.getSession().getAttribute("loginUser");
        //文章标题
        blogs.setTitle(request.getParameter("title"));
        //文章内容
        blogs.setText(request.getParameter("content"));

        blogs.setTypes(request.getParameter("types"));
        blogService.updateBlog(blogs);

        rabbitTemplate.convertAndSend("qs.blog", "qs.update", "update");
        return "redirect:/myBlogs";
    }


    //根据id删除博客
    @RequestMapping("/deleteBlog/{id}")
    public String deleteBlog(@PathVariable("id") Integer id, HttpServletRequest request) {
        Integer user_id = (Integer) request.getSession().getAttribute("loginUserId");
        blogService.deleteBlog(id, user_id);
        rabbitTemplate.convertAndSend("qs.blog", "qs.update", "update");
        return "redirect:/myBlogs";
    }

    //将type存入session
    @RequestMapping("/blogTypes/{type}")
    public String blogType(@PathVariable("type") String type, HttpServletRequest request) {
        request.getSession().setAttribute("type", type);
        return "redirect:/type";
    }

    //将type存入session(我的博客)
    @RequestMapping("/myBlogTypes/{type}")
    public String myBlogType(@PathVariable("type") String type, HttpServletRequest request) {
        request.getSession().setAttribute("userType", type);
        return "redirect:/myBlogType";
    }

    /**
     * 根据分类查询博客
     *
     * @param model
     * @return
     */
    @RequestMapping("/type")
    public String blogTypes(@RequestParam(defaultValue = "1") Integer pageIndex, Model model, HttpServletRequest request) {
        String type = (String) request.getSession().getAttribute("type");
        PageHelper.startPage(pageIndex, 5);
        List<UserBlog> userBlogs = blogService.findByUserBlog(type);
        PageInfo<UserBlog> pageInfo = new PageInfo<>(userBlogs, 5);
        int count = blogService.blogsTyepCount(type);
        String blogType = new String();
        if (type.equals("1")) {
            blogType = "科技类型";
        } else if (type.equals("2")) {
            blogType = "情感类型";
        } else if (type.equals("3")) {
            blogType = "人生经历";
        } else if (type.equals("4")) {
            blogType = "生活小妙招";
        } else if (type.equals("5")) {
            blogType = "娱乐文化";
        } else if (type.equals("6")) {
            blogType = "其他";
        }
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("count", count);
        model.addAttribute("type", blogType);
        model.addAttribute("userBlogs", userBlogs);
        return "typesBlog";
    }

    /**
     * 根据分类查询我的博客
     *
     * @param model
     * @return
     */
    @RequestMapping("/myBlogType")
    public String myBlogTypes(@RequestParam(defaultValue = "1") Integer pageIndex, Model model, HttpServletRequest request) {
        String type = (String) request.getSession().getAttribute("userType");
        Integer user_id = (Integer) request.getSession().getAttribute("loginUserId");
        PageHelper.startPage(pageIndex, 5);
        List<UserBlog> userBlogs = blogService.findByMyBlog(type, user_id);
        PageInfo<UserBlog> pageInfo = new PageInfo<>(userBlogs, 5);
        int count = blogService.myBlogsTyepCount(type, user_id);
        String blogType = new String();
        if (type.equals("1")) {
            blogType = "科技类型";
        } else if (type.equals("2")) {
            blogType = "情感类型";
        } else if (type.equals("3")) {
            blogType = "人生经历";
        } else if (type.equals("4")) {
            blogType = "生活小妙招";
        } else if (type.equals("5")) {
            blogType = "娱乐文化";
        } else if (type.equals("6")) {
            blogType = "其他";
        }
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("count", count);
        model.addAttribute("type", blogType);
        model.addAttribute("userBlogs", userBlogs);
        return "myTypesBlog";
    }

}


