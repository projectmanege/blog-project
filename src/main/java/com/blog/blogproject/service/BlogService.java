package com.blog.blogproject.service;


import com.blog.blogproject.bean.Blogs;
import com.blog.blogproject.bean.User;
import com.blog.blogproject.bean.UserBlog;
import com.github.pagehelper.PageInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface BlogService {

    //分页查询所有博客
    public PageInfo getPageBlogList(Integer currentUserId, Integer pageIndex, Integer pageSize);

    //查询所有博客
    public List<UserBlog> selectAllBlogs(Integer currentUserId);

    //添加博客
    public void addBlog(Blogs blogs);

    //点赞
    public UserBlog likes(Integer id);

    //根据id删除文章
    public void deleteBlog(Integer id, Integer user_id);

    //根据title或用户名或用户id查询博客
    public ArrayList<Map<String, Object>> findBySearch(String search, Integer page) throws IOException;

    //根据title或用户名或用户id查询用户
    public List<User> findByUserSearch(String search);

    //文章修改
    public void updateBlog(Blogs blogs);

    //博客计数
    public int blogsCount(Integer user_id);

    /**
     * 博客分类计数
     *
     * @param types
     * @return
     */
    int blogsTyepCount(String types);

    //我的博客分类计数
    int myBlogsTyepCount(String types, Integer user_id);

    //搜索结果
    public int searchCount(String search) throws IOException;

    //搜索用户结果
    public int searchUserCount(String search);

    //通过id查找博客
    public UserBlog selectBlogById(Integer id);

    //查询精选文章
    public PageInfo selectByIsSelected(int isSelected, Integer pageIndex, Integer pageSize);

    //精选文章计数统计
    public int isSelectedCount();

    UserBlog readAdd(Integer blogId);

    /**
     * 根据分类查询博客
     *
     * @param types
     * @return
     */
    List<UserBlog> findByUserBlog(String types);

    /**
     * 根据分类查询我的博客
     *
     * @param types
     * @return
     */
    List<UserBlog> findByMyBlog(String types, Integer user_id);

    /**
     * 赋值不同类型的图片路径
     *
     * @param type
     * @return
     */
    public String typeImg(String type);
}
