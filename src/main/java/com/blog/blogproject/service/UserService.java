package com.blog.blogproject.service;

import com.blog.blogproject.bean.User;
import org.apache.ibatis.annotations.Insert;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UserService {

    /**
     * 查询所有
     *
     * @return
     */
    List<User> findAll();

    /**
     * 根据id查询用户
     *
     * @return
     */
    User findById(Integer id);

    /**
     * 根据账户名查找该账户名数量
     *
     * @param user_id
     * @return
     */
    Integer userNumber(Integer user_id);

    /**
     * 根据邮箱查找用户
     *
     * @param email
     * @return
     */
    User findByemail(String email);

    /**
     * 查找邮箱是否已经注册
     *
     * @param email
     * @return
     */
    Integer emailNumber(String email);

    /**
     * 新增用户
     *
     * @param file
     * @param user
     * @param year
     * @param month
     * @param day
     */
    void insertUser(MultipartFile file, User user, String year, String month, String day);

    /**
     * 新增没有填写生日的用户
     *
     * @param file
     * @param user
     * @param year
     * @param month
     * @param day
     */
    void insertUserWithoutBirthday(MultipartFile file, User user, String year, String month, String day);

    /**
     * 根据id删除账户
     *
     * @param id
     */
    void deleteUserById(Integer id);

    /**
     * 根据用户账号和用户名查询用户
     *
     * @param userId
     * @return
     */
    List<User> findUserByUserIdOrName(String userId);

    /**
     * 更新用户
     *
     * @param file
     * @param user
     * @param year
     * @param month
     * @param day
     * @return
     */
    User updateUser(MultipartFile file, Integer user_id, User user, String year, String month, String day);

    /**
     * 修改密码
     *
     * @param user_id
     * @param password
     * @return
     */
    User updatePwd(Integer user_id, String password);

}
