package com.blog.blogproject.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * 每天定时更新redis和Elastic Search
 */
@Service
public class ScheduledService {

    @Autowired
    UpdateService updateService;

    Logger logger = LoggerFactory.getLogger(getClass());

    @Scheduled(cron = "0 0 0,6,12,18 * * *")
    public void updateDate() {
        updateService.clearRedis();
        updateService.clearEs();
        logger.info("执行clear");
    }

}
