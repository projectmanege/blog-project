package com.blog.blogproject.service.impl;

import com.blog.blogproject.bean.Comment;
import com.blog.blogproject.mapper.CommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {

    @Autowired
    CommentMapper commentMapper;

    public void commentAdd(Integer currentUserId, String commentTxt, Integer blogId) {
        Comment comment = new Comment();
        comment.setText(commentTxt);
        comment.setBlogsId(blogId);
        comment.setUserId(currentUserId);
        comment.setLikes(0);
        commentMapper.insert(comment);
    }

    public List<Comment> findCommentByUserAndBlog(Integer blogId) {
        List<Comment> comments = commentMapper.findCommentByUserAndBlog(blogId);
        return comments;
    }

    public void addNice(Integer commentId) {
        commentMapper.addNice(commentId);
    }

    public void delComment(Integer commentId) {
        commentMapper.deleteByPrimaryKey(commentId);
    }

    public String findAvatar(Integer id) {
        String avatar = commentMapper.selectAvatarByCommentId(id);
        return avatar;
    }

    public String selectUserNameByCommentId(Integer id) {
        return commentMapper.selectUserNameByCommentId(id);
    }
}
