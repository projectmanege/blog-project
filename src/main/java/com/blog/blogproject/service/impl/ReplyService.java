package com.blog.blogproject.service.impl;

import com.blog.blogproject.bean.Reply;
import com.blog.blogproject.mapper.ReplyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReplyService {

    @Autowired
    ReplyMapper replyMapper;

    public void replyAdd(String replyText, Integer userID, String userName, Integer commentId) {
        Reply reply = new Reply();
        reply.setCommentId(commentId);
        reply.setUserId(userID);
        reply.setUserName(userName);
        reply.setReplyText(replyText);
        replyMapper.insert(reply);
    }

    public List<Reply> findReplyByCommentId(Integer commentId) {
        List<Reply> replyList = replyMapper.findReplyByCommentId(commentId);
        return replyList;
    }

    public String findAvatarByReplyId(Integer replyId) {
        String replyAvatar = replyMapper.findAvatarByReplyId(replyId);
        return replyAvatar;
    }

    public String selectUserNameByReplyId(Integer id) {
        return replyMapper.selectUserNameByReplyId(id);
    }
}
