package com.blog.blogproject.service.impl;

import com.blog.blogproject.Repository.UserRepository;
import com.blog.blogproject.bean.User;
import com.blog.blogproject.mapper.UserMapper;
import com.blog.blogproject.service.UserService;
import com.blog.blogproject.utils.CacheRemove;
import com.blog.blogproject.utils.EmailCodeUtils;
import com.blog.blogproject.utils.UploadFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Service("userSerivce")
public class UserServiceImpl implements UserService {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    UserMapper userMapper;

    @Autowired
    JavaMailSenderImpl mailSender;

    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    @Autowired
    UserRepository userRepository;

    /**
     * 监听用户注册发送邮箱验证码
     *
     * @param email
     * @return
     */
    @RabbitListener(queues = "blog.addUser")
    public void sendMail(String email) {
        System.out.println(email);
        EmailCodeUtils emailCodeUtils = new EmailCodeUtils();
        String code = emailCodeUtils.getNumber();
        String context = "此次注册验证码为：" + code + ",有效期为五分钟";
        emailCodeUtils.sendEmailCode("1197929824@qq.com", email, "blog注册验证", context, mailSender);
        redisTemplate.opsForValue().set(email, code, 5, TimeUnit.MINUTES);
        logger.info("发送成功");
    }

    /**
     * 查询所有
     *
     * @return
     */
    @Override
    public List<User> findAll() {
//        return userMapper.findAll();
        return userRepository.findAll();
    }

    /**
     * 根据id查询用户
     *
     * @return
     */
    @Override
    @Caching(
            cacheable = {
                    @Cacheable(cacheNames = "user", key = "#root.args[0]")
            },
            put = {
                    @CachePut(cacheNames = "user", key = "#result.user_id"),
                    @CachePut(cacheNames = "user", key = "#result.email")
            }
    )
    public User findById(Integer id) {
        return userMapper.findById(id);
    }

    @Override
    public User findByemail(String email) {
        return userMapper.findByemail(email);
    }

    /**
     * 查找邮箱是否已注册
     *
     * @param email
     * @return
     */
    @Override
    public Integer emailNumber(String email) {
        return userMapper.emailNumber(email);
    }

    /**
     * 根据账户名查找该账户名数量
     *
     * @param user_id
     * @return
     */
    @Override
    public Integer userNumber(Integer user_id) {
        return userMapper.userNumber(user_id);
    }

    /**
     * 新增用户
     *
     * @param file
     * @param user
     * @param year
     * @param month
     * @param day
     */
    @Override
    public void insertUser(MultipartFile file, User user, String year, String month, String day) {
        util(file, user, year, month, day, 1);
        userMapper.insertUser(user);
        userRepository.save(userMapper.findById(user.getUser_id()));
    }

    /**
     * 新增没有填写生日的用户
     *
     * @param file
     * @param user
     * @param year
     * @param month
     * @param day
     */
    @Override
    public void insertUserWithoutBirthday(MultipartFile file, User user, String year, String month, String day) {
        util(file, user, year, month, day, 1);
        userMapper.insertUserWithoutBirthday(user);
        userRepository.save(userMapper.findById(user.getUser_id()));
    }

    /**
     * 根据id删除用户
     *
     * @param id
     */
    @Override
    public void deleteUserById(Integer id) {
        userMapper.deleteUserById(id);
    }

    /**
     * 根据用户账号和用户名查询用户
     *
     * @param userId
     * @return
     */
    @Override
    public List<User> findUserByUserIdOrName(String userId) {
        List<User> users;
        users = userMapper.findUserByUesrIdOrName(userId);
        return users;
    }

    /**
     * 更新用户
     *
     * @param file
     * @param user
     * @param year
     * @param month
     * @param day
     * @return
     */
    @CacheRemove({"'pageBlog::'+#user_id+'-*'", "selectBlog::*", "'blogCount::'+#user_id", "selectBlogCount::1"})
    @Override
    public User updateUser(MultipartFile file, Integer user_id, User user, String year, String month, String day) {
        logger.info("更新用户信息");
        util(file, user, year, month, day, 2);
        userMapper.updateUser(user);
        userRepository.save(user);
        return userMapper.findById(user_id);
    }

    /**
     * 修改密码
     *
     * @param user_id
     * @param password
     * @return
     */
    @Override
    public User updatePwd(Integer user_id, String password) {
        User user = userMapper.findById(user_id);
        user.setPassword(password);
        userRepository.save(user);
        userMapper.updatePwd(user_id, password);
        return userMapper.findById(user_id);
    }

    /**
     * 新增和更新用户的重复代码
     *
     * @param file
     * @param user
     * @param year
     * @param month
     * @param day
     */
    private void util(MultipartFile file, User user, String year, String month, String day, Integer flag) {
        String avatar;
        avatar = UploadFile.uploadFile(file, "/home/tomcat/avatars/");
        if (avatar == null) {
            if (flag == 1) {
                avatar = "asserts/avatars/avatar.jpg";
            }
            if (flag == 2) {
                User user_get_id = userMapper.findById(user.getUser_id());
                avatar = user_get_id.getAvatar();
            }
        }
        if (!year.isEmpty() && !month.isEmpty() && !day.isEmpty()) {
            String s = year + "-" + month + "-" + day;
            java.sql.Date birthday = java.sql.Date.valueOf(s);
            user.setBirthday(birthday);
        } else {
            user.setBirthday(null);
        }
        user.setAvatar(avatar);
    }

}
