package com.blog.blogproject.service.impl;

import com.blog.blogproject.Repository.UserBlogRepository;
import com.blog.blogproject.bean.UserBlog;
import com.blog.blogproject.mapper.BlogMapper;
import com.blog.blogproject.utils.CacheRemove;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class UpdateService {

    @Autowired
    UserBlogRepository blogRepository;

    @Autowired
    BlogMapper blogMapper;

    @Autowired
    RestHighLevelClient elasticsearchClient;

    Logger logger = LoggerFactory.getLogger(getClass());

    @CacheEvict(cacheNames = "pageBlog", key = "1-1")
    @CacheRemove({"pageBlog::*", "selectBlog::*"})
    public void clearRedis() {
        logger.info("clearRedis");
    }

    @RabbitListener(queues = "blog.updateES")
    public void clearEs() {
        blogRepository.deleteAll();
        List<UserBlog> blogs = blogMapper.selectAll();
        for (UserBlog blog : blogs) {
            blogRepository.save(blog);
        }
        logger.info("clearEs");
    }

//    @RabbitListener(queues = "blog.updateES")
//    public void updateEs() throws IOException {
//        BulkRequest bulkRequest = new BulkRequest();
//        bulkRequest.add(
//                new DeleteRequest("qs","")
//        );
//
//        BulkResponse bulkResponse = elasticsearchClient.bulk(bulkRequest, RequestOptions.DEFAULT);
//
//        logger.info("updateEs");
//    }
}
