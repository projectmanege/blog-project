package com.blog.blogproject.service.impl;

import com.blog.blogproject.Repository.UserBlogRepository;
import com.blog.blogproject.bean.Blogs;
import com.blog.blogproject.bean.User;
import com.blog.blogproject.bean.UserBlog;
import com.blog.blogproject.component.JsonDateDeserializer;
import com.blog.blogproject.mapper.BlogMapper;
import com.blog.blogproject.service.BlogService;
import com.blog.blogproject.utils.CacheRemove;
import com.blog.blogproject.utils.DateFormat;
import com.blog.blogproject.utils.DelTagsUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class BlogServiceImpl implements BlogService {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    public BlogMapper blogMapper;

    @Autowired
    UserBlogRepository blogRepository;

    @Autowired
    RestHighLevelClient elasticsearchClient;

    @Autowired
    JsonDateDeserializer jsonDateDeserializer;

    @Autowired
    UpdateService updateService;

    //分页查询所有博客
    @Override
    @Cacheable(cacheNames = "pageBlog", key = "#root.args[0]+'-'+#root.args[1]")
    public PageInfo getPageBlogList(Integer currentUserId, Integer pageIndex, Integer pageSize) {
        logger.info("分页博客");
        PageHelper.startPage(pageIndex, pageSize);
        List<UserBlog> blogsList = blogMapper.selectAllBlogs(currentUserId);
//        List<Blogs> blogsList = blogDao.selectAllBlogs(user_id);
        //将html格式转成文本格式
        for (UserBlog blogs : blogsList) {
            String html = HtmlUtils.htmlUnescape(blogs.getText());
            blogs.setText(DelTagsUtil.getTextFromHtml(html));
            blogs.setImgs(typeImg(blogs.getTypes()));
        }
        return new PageInfo<>(blogsList);
    }

    //查询精选文章
    @Cacheable(cacheNames = "selectBlog", key = "#root.args[1]")
    @Override
    public PageInfo selectByIsSelected(int isSelected, Integer pageIndex, Integer pageSize) {
        logger.info("精选文章");
        List<UserBlog> blogsList = blogMapper.selectByIsSelected(isSelected);
        PageHelper.startPage(pageIndex, pageSize);
        //将html格式转成文本格式
        for (UserBlog blogs : blogsList) {
            String html = HtmlUtils.htmlUnescape(blogs.getText());
            blogs.setText(DelTagsUtil.getTextFromHtml(html));
            blogs.setImgs(typeImg(blogs.getTypes()));
        }

        return new PageInfo<>(blogsList);
    }

    //精选文章计数统计
    @Override
    @Cacheable(cacheNames = "selectBlogCount", key = "1")
    public int isSelectedCount() {
        logger.info("精选统计");
        return blogMapper.isSelectedCount();
    }

    //根据Id查询博客
    @Override
    public UserBlog selectBlogById(Integer id) {
        logger.info("博客原文");
        UserBlog blog = blogMapper.selectBlogById(id);
        blog.setImgs("../" + typeImg(blog.getTypes()));
        return blog;
    }

    //添加博客
    @CacheRemove({"'pageBlog::'+#blogs.user_id+'-*'", "selectBlog::*", "'blogCount::'+#blogs.user_id", "selectBlogCount::1"})
    @Override
    public void addBlog(Blogs blogs) {
        logger.info("添加博客");
        blogs.setLikes(0);
        blogs.setIsSelected("1");
        blogs.setNumber_of_readers(0);

        String html = HtmlUtils.htmlEscapeHex(blogs.getText());
        blogs.setText(html);

        blogMapper.addBlog(blogs);

    }

    //删除博客
    @CacheRemove({"'pageBlog::'+#userId+'-*'", "selectBlog::*", "'blogCount::'+#userId", "selectBlogCount::1"})
    @Override
    public void deleteBlog(Integer id, Integer userId) {
        logger.info("删除博客");
        blogMapper.delectBlog(id);
    }

    //点赞博客
    @Override
    public UserBlog likes(Integer id) {
        logger.info("点赞博客");
        blogMapper.likes(id);
        return blogMapper.selectBlogById(id);
    }

    //浏览次数
    @Override
    public UserBlog readAdd(Integer blogId) {
        logger.info("浏览博客");
        blogMapper.readAdd(blogId);
        return blogMapper.selectBlogById(blogId);
    }

    //更新博客
    @CacheRemove({"'pageBlog::'+#blogs.user_id+'-*'", "selectBlog::*", "'blogCount::'+#blogs.user_id", "selectBlogCount::1"})
    @Override
    public void updateBlog(Blogs blogs) {
        logger.info("更新博客");
        String html = HtmlUtils.htmlEscapeHex(blogs.getText());
        blogs.setText(html);
        blogMapper.updateBlog(blogs);
    }

    //博客计数
    @Override
    @Cacheable(cacheNames = "blogCount")
    public int blogsCount(Integer user_id) {
        logger.info("博客计数");
        return blogMapper.blogsCount(user_id);
    }

    //根据title或用户名或用户id查询博客
    @Override
    public ArrayList<Map<String, Object>> findBySearch(String search, Integer page) throws IOException {
////        List<UserBlog> blogsList = blogMapper.selectBySearch(search);
//        Page<UserBlog> blogsList = null;
//        //将html格式转成文本格式
//        for (UserBlog blogs:blogsList){
//            String html = HtmlUtils.htmlUnescape(blogs.getText());
//            blogs.setText(DelTagsUtil.getTextFromHtml(html));
//            blogs.setImgs(typeImg(blogs.getTypes()));
//        }
//        return blogsList;

        //创建搜索请求对象，并设置查询指定的某个文档库
        SearchRequest searchRequest = new SearchRequest("qs");
        //创建搜索内容参数设置对象：SearchSourceBuilder
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
//        MatchQueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("title", search);
        //构建高亮体
//        HighlightBuilder highlightBuilder = new HighlightBuilder();
//        highlightBuilder.preTags("<span style=\"color:red\">");
//        highlightBuilder.postTags("</span>");
//        highlightBuilder.requireFieldMatch(false);  //多个高亮显示
//        //高亮字段
//        highlightBuilder.field("title").field("user_name").field("text");
        //为搜索的文档内容对象SearchSourceBuilder设置参数
        sourceBuilder.query(QueryBuilders.multiMatchQuery(search, "title", "user_name", "text"));
//        sourceBuilder.highlighter(highlightBuilder);
        sourceBuilder.from((page - 1) * 5);
        sourceBuilder.size(5);
        SearchResponse searchResponse;
        //将SourceBuilder对象添加到搜索请求中
        searchRequest.source(sourceBuilder);
        //使用高级客户端获取搜索结果
        searchResponse = elasticsearchClient.search(searchRequest, RequestOptions.DEFAULT);

        ArrayList<Map<String, Object>> list = new ArrayList<>();
        for (SearchHit hit : searchResponse.getHits().getHits()) {
//            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
//            org.elasticsearch.search.fetch.subphase.highlight.HighlightField title = highlightFields.get("title");
//            Map<String, Object> soursAsMap = hit.getSourceAsMap();      //原来的结果
//
//            //解析高亮的字段，将原来的字段换为我们高亮的字段即可！
//            if (title != null) {
//                Text[] fragments = title.fragments();
//                String n_title = "";
//                for (Text text : fragments) {
//                    n_title += text;
//                }
//                soursAsMap.put("title", n_title);
//            }

            list.add(hit.getSourceAsMap());
        }

        for (Map<String, Object> blogs : list) {
            String html = HtmlUtils.htmlUnescape(String.valueOf(blogs.get("text")));
            blogs.put("create_time", new DateFormat().format(blogs.get("create_time")));
            blogs.put("text", DelTagsUtil.getTextFromHtml(html));
            blogs.put("imgs", typeImg(String.valueOf(blogs.put("types", blogs.get("types")))));
        }
        return list;
    }

    //搜索博客计数
    @Override
    public int searchCount(String search) throws IOException {
//        return blogMapper.searchCount(search);
        SearchRequest searchRequest = new SearchRequest("qs");
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(QueryBuilders.multiMatchQuery(search, "title", "user_name", "text"));
        SearchResponse searchResponse;
        searchRequest.source(sourceBuilder);
        searchResponse = elasticsearchClient.search(searchRequest, RequestOptions.DEFAULT);
        int count = (int) searchResponse.getHits().getTotalHits().value;

//        for (SearchHit hit : searchResponse.getHits()) {
//            count++;
//        }

        return count;
    }

    //根据title或用户名或用户id查询用户
    @Override
    public List<UserBlog> selectAllBlogs(Integer currentUserId) {
        List<UserBlog> blogsList = blogMapper.selectAllBlogs(currentUserId);
        //将html格式转成文本格式
        for (UserBlog blogs : blogsList) {
            String html = HtmlUtils.htmlUnescape(blogs.getText());
            blogs.setText(DelTagsUtil.getTextFromHtml(html));
            blogs.setImgs(typeImg(blogs.getTypes()));
        }
        return blogsList;
    }

    /**
     * 根据分类查询博客
     *
     * @param types
     * @return
     */
    @Override
    public List<UserBlog> findByUserBlog(String types) {
        List<UserBlog> blogsList = blogMapper.findByUserBlog(types);
        for (UserBlog blogs : blogsList) {
            String html = HtmlUtils.htmlUnescape(blogs.getText());
            blogs.setText(DelTagsUtil.getTextFromHtml(html));
            blogs.setImgs(typeImg(blogs.getTypes()));
        }
        return blogsList;
    }

    //分类博客计数
    @Override
    public int blogsTyepCount(String types) {
        return blogMapper.blogsTyepCount(types);
    }

    //分类我的博客计数
    @Override
    public int myBlogsTyepCount(String types, Integer user_id) {
        return blogMapper.mybBlogsTyepCount(types, user_id);
    }

    /**
     * 根据分类查找我的博客
     *
     * @param types
     * @param user_id
     * @return
     */
    @Override
    public List<UserBlog> findByMyBlog(String types, Integer user_id) {
        List<UserBlog> blogsList = blogMapper.findByMyBlog(types, user_id);
        for (UserBlog blogs : blogsList) {
            String html = HtmlUtils.htmlUnescape(blogs.getText());
            blogs.setText(DelTagsUtil.getTextFromHtml(html));
            blogs.setImgs(typeImg(blogs.getTypes()));
        }
        return blogsList;
    }

    /**
     * 搜索用户
     *
     * @param search
     * @return
     */
    @Override
    public List<User> findByUserSearch(String search) {
        return blogMapper.selectUserBySearch(search);
    }

    //搜索用户计数
    @Override
    public int searchUserCount(String search) {
        return blogMapper.searchUserCount(search);
    }

    /**
     * 赋值不同类型的图片路径
     *
     * @param type
     * @return
     */
    @Override
    public String typeImg(String type) {
        String typeImgs = new String();
        if (type.equals("1")) {
            typeImgs = "asserts/blogTypsImg/technology.png";
        } else if (type.equals("2")) {
            typeImgs = "asserts/blogTypsImg/emotion.png";
        } else if (type.equals("3")) {
            typeImgs = "asserts/blogTypsImg/experience.png";
        } else if (type.equals("4")) {
            typeImgs = "asserts/blogTypsImg/lifeIdeas.png";
        } else if (type.equals("5")) {
            typeImgs = "asserts/blogTypsImg/entertainment.png";
        } else if (type.equals("6")) {
            typeImgs = "asserts/blogTypsImg/other.png";
        }
        return typeImgs;
    }

}
