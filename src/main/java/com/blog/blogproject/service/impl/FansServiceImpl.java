package com.blog.blogproject.service.impl;


import com.blog.blogproject.bean.FanedInf;
import com.blog.blogproject.bean.Fans;
import com.blog.blogproject.mapper.FansMapper;
import com.blog.blogproject.service.FansService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FansServiceImpl implements FansService {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    FansMapper fansMapper;

    /**
     * 查找所有粉丝
     *
     * @param user_id
     * @return
     */
    @Override
    public List<FanedInf> ShowAllFans(Integer user_id) {
        return fansMapper.ShowAllFans(user_id);
    }


    public List<FanedInf> findById(Integer user_id) {
        return fansMapper.findById(user_id);
    }


    //关注用户
    @Caching(
            evict = {
                    @CacheEvict(cacheNames = "fansCount", key = "#root.args[0]"),
                    @CacheEvict(cacheNames = "fanedCount", key = "#root.args[1]")
            }
    )
    @Override
    public void insertFans(Integer currentUserId, Integer fansId) {
        logger.info("关注用户");
        Fans fans = new Fans();
        fans.setFans_id(currentUserId);
        fans.setUser_id(fansId);
        fansMapper.insertFans(fans);
    }

    //取消关注
    @Caching(
            evict = {
                    @CacheEvict(cacheNames = "fansCount", key = "#root.args[0]"),
                    @CacheEvict(cacheNames = "fanedCount", key = "#root.args[1]"),
                    @CacheEvict(cacheNames = "user", key = "#root.args[1]"),
                    @CacheEvict(cacheNames = "user", key = "#root.args[0]")
            }
    )
    @Override
    public void deleteFaned(Integer user_id, Integer fans_id) {
        fansMapper.deleteFans(user_id, fans_id);
    }

    //移除粉丝
    @Caching(
            evict = {
                    @CacheEvict(cacheNames = "fansCount", key = "#root.args[1]"),
                    @CacheEvict(cacheNames = "fanedCount", key = "#root.args[0]"),
                    @CacheEvict(cacheNames = "user", key = "#root.args[1]"),
                    @CacheEvict(cacheNames = "user", key = "#root.args[0]")
            }
    )
    @Override
    public void deleteFans(Integer user_id, Integer fans_id) {
        fansMapper.deleteFans(user_id, fans_id);
    }

    //粉丝数量
    @Cacheable(cacheNames = "fansCount", key = "#root.args[0]")
    public int countFans(Integer user_id) {
        logger.info("粉丝数量");
        return fansMapper.countFans(user_id);
    }

    //关注数量
    @Cacheable(cacheNames = "fanedCount", key = "#root.args[0]")
    public int countFaned(Integer user_id) {
        logger.info("关注数量");
        return fansMapper.countFaned(user_id);
    }

    @Override
    public int count(Integer currentUserId, Integer user_id) {
        return fansMapper.count(currentUserId, user_id);
    }
}
