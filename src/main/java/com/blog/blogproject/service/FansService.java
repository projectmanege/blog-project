package com.blog.blogproject.service;

import com.blog.blogproject.bean.FanedInf;

import java.util.List;

public interface FansService {
    List<FanedInf> ShowAllFans(Integer user_id);

    List<FanedInf> findById(Integer user_id);

    void insertFans(Integer currentUserId, Integer fansId);

    void deleteFaned(Integer user_id, Integer fans_id);

    void deleteFans(Integer user_id, Integer fans_id);

    int countFans(Integer user_id);

    int countFaned(Integer user_id);

    int count(Integer currentUserId, Integer user_id);
}
