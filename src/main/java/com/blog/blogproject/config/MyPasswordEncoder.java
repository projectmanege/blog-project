package com.blog.blogproject.config;

public class MyPasswordEncoder implements org.springframework.security.crypto.password.PasswordEncoder {

    @Override
    public String encode(CharSequence charSequence) {
        return (String) charSequence;
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        return true;
    }

    @Override
    public boolean upgradeEncoding(String encodedPassword) {
        return true;
    }
}
