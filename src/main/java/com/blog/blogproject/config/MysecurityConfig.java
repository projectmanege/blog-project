package com.blog.blogproject.config;

import com.blog.blogproject.component.BeforeLoginFilter;
import com.blog.blogproject.component.MyUserDetailService;
import com.blog.blogproject.component.RedisPersistemRe;
import com.blog.blogproject.component.RememberUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.csrf.CsrfFilter;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class MysecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private MyUserDetailService myUserDetailService;

    @Autowired
    RedisPersistemRe redisPerSisRe;

    @Autowired
    DataSource dataSource;

//    @Autowired
//    RememberUserDetailService rememberUserDetailService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //super.configure(http);
        //定制请求的授权规则
        http.authorizeRequests().antMatchers("/").permitAll()
                .antMatchers("/create", "/user/sendEmailCode", "/user/create", "/user/insertUser", "/user/sendUpdateEmail", "/user/updatePwd", "/user/checkEmail", "/forgetPwd").permitAll()
                .antMatchers("/asserts/**", "/ueditor/**").permitAll()
                .antMatchers("/webjars/**").permitAll()
                .antMatchers("/verification").permitAll()
                .antMatchers("/user/login").permitAll()
                .antMatchers("/login", "/login.html").permitAll()
                .antMatchers("/**").hasAnyAuthority("USER")
                .and().headers().frameOptions().disable();

        //开启自动配置的登陆功能，效果，如果没有登录，没有权限就会来到登录页面
        http.formLogin().usernameParameter("user_id").passwordParameter("password")
                .loginProcessingUrl("/user/login")
                .defaultSuccessUrl("/main")
//                .failureForwardUrl("")
                .loginPage("/login")
                .and().csrf().disable();
        //1、/login来到登陆页
        //2、重定向到/login?error表示登陆失败
        //3、更多详细规定
        //4、默认post形式的/login代表处理登录
        //5、一旦定制loginPage；那么 loginPage 的post请求就是登陆

        //开启自动配置的注销功能
        http.logout().logoutUrl("/user/loginOut").logoutSuccessUrl("/");//注销成功以后来到首页
        //1、访问 /logout 表示用户注销，清空session
        //2、注销成功会返回 /login?logout 页面

        //开启记住我功能
        http.rememberMe()
                .rememberMeParameter("remember-me")
                .userDetailsService(myUserDetailService)
                .tokenValiditySeconds(2592000)
                //指定记住登录信息所使用的数据源
                .tokenRepository(rememberMeServices());
        //登陆成功以后，将cookie发给浏览器保存，以后访问页面带上这个cookie，只要通过检查就可以免登录
        //点击注销会删除cookie

//        http.addFilterBefore(new BeforeLoginFilter(), CsrfFilter.class);
    }

    //定义认证规则
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    //super.configure(auth);
//        auth.inMemoryAuthentication()
//                .withUser("1").password("1").roles("VIP");
//        auth.jdbcAuthentication();
//
//    }


    //注入数据源
    @Bean
    public JdbcTokenRepositoryImpl rememberMeServices() {
        JdbcTokenRepositoryImpl rememberMeTokenRepository = new JdbcTokenRepositoryImpl();
        rememberMeTokenRepository.setDataSource(dataSource);
        return rememberMeTokenRepository;
    }
}
