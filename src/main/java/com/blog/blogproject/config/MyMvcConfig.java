package com.blog.blogproject.config;

import com.blog.blogproject.component.LoginHandlerInterceptor;
import com.blog.blogproject.component.MyLocalReolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyMvcConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("login");
        registry.addViewController("/login.html").setViewName("login");
    }

    //注册拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //super.addInterceptors(registry)
        //SpringBoot已经做好了静态资源的映射
        registry.addInterceptor(new LoginHandlerInterceptor()).addPathPatterns("/**")
                .excludePathPatterns("/login.html", "/", "/user/login", "/asserts/**", "/webjars/**", "/user/create", "/ueditor/**", "/user/insertUser", "/create", "/user/loginOut", "/verification", "/user/sendEmailCode");
    }

    /**
     * 开放静态资源
     *
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("classpath:/resources/");
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/public/**").addResourceLocations("classpath:/public/");
        registry.addResourceHandler("/templates/**").addResourceLocations("classpath:/templates/");
        String path = System.getProperty("user.dir") + "\\src\\main\\resources\\static\\asserts\\avatars\\";
        registry.addResourceHandler("/asserts/avatars/**").addResourceLocations("file:" + path);
    }

    @Bean
    public LocaleResolver localeResolver() {
        return new MyLocalReolver();
    }
}
