package com.blog.blogproject.utils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;

@Target({METHOD})   //说明了Annotation所修饰的对象范围
@Retention(RetentionPolicy.RUNTIME)
//可以用来修饰注解，是注解的注解，称为元注解。RetentionPolicy.RUNTIME：注解不仅被保存到class文件中，jvm加载class文件之后，仍然存在
public @interface CacheRemove {

    String[] value() default {};
}
