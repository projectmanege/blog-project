package com.blog.blogproject.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

/**
 * 邮箱验证码工具类
 */
public class EmailCodeUtils {

    /**
     * 生成6位随机验证码
     *
     * @return
     */
    public String getNumber() {
        String str = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String code = "";
        for (int i = 0; i < 6; i++) {
            int index = (int) (Math.random() * str.length());
            code += str.charAt(index);
        }
        return code;
    }

    /**
     * 发送邮箱验证码
     *
     * @param receiverEmail
     * @param subject
     * @param msg
     */
    public void sendEmailCode(String EMAIL_FORM_MAIL, String receiverEmail,
                              String subject, String msg, JavaMailSenderImpl mailSender) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(receiverEmail);
        message.setFrom(EMAIL_FORM_MAIL);
        message.setSubject(subject);
        message.setText(msg);
        mailSender.send(message);
    }

}

