package com.blog.blogproject.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public class UploadFile {

    public static String uploadFile(MultipartFile file, String uploadPath) {
        //如果文件是空，返回空
        if (file.isEmpty()) {
            return null;
        }

        //获取这个文件的名字，目的是获取这个文件的后缀名
        String fileName = file.getOriginalFilename();
        //获取后缀名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        //为上传的文件建立一个新名字，避免文件名重复。这里用当前时间戳 加 一个6位随机数 加 文件后缀名
        fileName = System.currentTimeMillis() + "" + ((int) (Math.random() * 1000000)) + suffixName;

        //在磁盘创建这个文件目录
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

        //创建这个绝对路径的文件对象
        File uploadFile = new File(uploadDir + uploadDir.separator + fileName);
        try {
            //把file转移给uploadFile
            file.transferTo(uploadFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //最后，这里没有直接返回绝对地址，而是返回了以“picture”开头的文件。
        return "http://47.115.140.130:8080/avatars/" + fileName;

    }
}
