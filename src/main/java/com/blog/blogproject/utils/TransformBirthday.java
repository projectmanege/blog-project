package com.blog.blogproject.utils;

import java.util.Date;
import java.text.SimpleDateFormat;

/**
 * 生日拆分为year，month，day三个字符串的工具类
 */
public class TransformBirthday {
    private String year;
    private String month;
    private String day;

    public TransformBirthday(Date birthday) {
        if (birthday != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String tfBirthday = sdf.format(birthday);
            String[] tfBirthday1 = tfBirthday.split("-");
            year = tfBirthday1[0];
            month = tfBirthday1[1];
            day = tfBirthday1[2];
        }
    }

    public String getYear() {
        return year;
    }

    public String getMonth() {
        return month;
    }

    public String getDay() {
        return day;
    }
}
