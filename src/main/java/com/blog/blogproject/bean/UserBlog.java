package com.blog.blogproject.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户关联博客实体类
 */
@Document(indexName = "qs")
public class UserBlog implements Serializable {

    private Integer id;                 //自动递增 123
    private Integer user_id;            //发表博客的用户的id(不是账号)
    private String user_name;           //发表博客的用户的用户名
    private String user_sex;            //用户性别
    private String types;               //博客分类
    private String title;               //博客标题
    private String text;                //博客内容
    private Date create_time;           //博客发表时间
    private Integer number_of_readers;  //阅读博客的人数
    private Integer likes;              //点赞博客的数量
    private String avatar;              //存放用户头像的路径
    private String isSelected;          //是否精选博客
    private Date birthday;            //用户生日
    private String email;          //电子邮箱
    private String phone_number;    //手机号码
    private String imgs;                //显示博客类别的

    public UserBlog() {
        super();
    }

    public UserBlog(Integer id, Integer user_id, String user_name, String user_sex, String types, String title, String text, Date create_time, Integer number_of_readers, Integer likes, String avatar, String isSelected, Date birthday, String email, String phone_number, String imgs) {
        this.id = id;
        this.user_id = user_id;
        this.user_name = user_name;
        this.user_sex = user_sex;
        this.types = types;
        this.title = title;
        this.text = text;
        this.create_time = create_time;
        this.number_of_readers = number_of_readers;
        this.likes = likes;
        this.avatar = avatar;
        this.isSelected = isSelected;
        this.birthday = birthday;
        this.email = email;
        this.phone_number = phone_number;
        this.imgs = imgs;
    }

    public String getImgs() {
        return imgs;
    }

    public void setImgs(String imgs) {
        this.imgs = imgs;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Integer getNumber_of_readers() {
        return number_of_readers;
    }

    public void setNumber_of_readers(Integer number_of_readers) {
        this.number_of_readers = number_of_readers;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(String isSelected) {
        this.isSelected = isSelected;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getUser_sex() {
        return user_sex;
    }

    public void setUser_sex(String user_sex) {
        this.user_sex = user_sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    @Override
    public String toString() {
        return "UserBlog{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", user_name='" + user_name + '\'' +
                ", user_sex='" + user_sex + '\'' +
                ", types='" + types + '\'' +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", create_time=" + create_time +
                ", number_of_readers=" + number_of_readers +
                ", likes=" + likes +
                ", avatar='" + avatar + '\'' +
                ", isSelected='" + isSelected + '\'' +
                ", birthday=" + birthday +
                ", email='" + email + '\'' +
                ", phone_number='" + phone_number + '\'' +
                ", imgs='" + imgs + '\'' +
                '}';
    }
}
