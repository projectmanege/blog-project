package com.blog.blogproject.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
//import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;
import java.util.Date;

/**
 * 博客实体类
 */
//@Document(indexName = "blog")
@Document(collection = "blogs")
public class Blogs implements Serializable {

    private Integer id;                 //自动递增 123
    private Integer user_id;            //发表博客的用户的id(不是账号)
    private String types;               //博客分类
    private String title;               //博客标题
    private String text;                //博客内容
    private Date create_time;           //博客发表时间
    private Integer number_of_readers;  //阅读博客的人数
    private Integer likes;              //点赞博客的数量
    private String imgs;                //存放博客图片的路径
    private String isSelected;          //是否精选博客

    public Blogs() {
        super();
    }

    public Blogs(Integer id, Integer user_id, String types, String title, String text, Date create_time, Integer number_of_readers, Integer likes, String imgs, String isSelected) {
        this.id = id;
        this.user_id = user_id;
        this.types = types;
        this.title = title;
        this.text = text;
        this.create_time = create_time;
        this.number_of_readers = number_of_readers;
        this.likes = likes;
        this.imgs = imgs;
        this.isSelected = isSelected;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Integer getNumber_of_readers() {
        return number_of_readers;
    }

    public void setNumber_of_readers(Integer number_of_readers) {
        this.number_of_readers = number_of_readers;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public String getImgs() {
        return imgs;
    }

    public void setImgs(String imgs) {
        this.imgs = imgs;
    }

    public String getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(String isSelected) {
        this.isSelected = isSelected;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    @Override
    public String toString() {
        return "Blogs{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", types='" + types + '\'' +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", create_time=" + create_time +
                ", number_of_readers=" + number_of_readers +
                ", likes=" + likes +
                ", imgs='" + imgs + '\'' +
                ", isSelected='" + isSelected + '\'' +
                '}';
    }
}
