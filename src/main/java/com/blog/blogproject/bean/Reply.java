package com.blog.blogproject.bean;

import java.util.Date;

public class Reply {
    private Integer id;

    private Integer commentId;

    private String replyText;

    private Integer userId;

    private Date createTime;

    private String replyAvatar;

    private String userName;

    public Reply(Integer id, Integer commentId, String replyText, Integer userId, String userName, Date createTime, String replyAvatar) {
        this.id = id;
        this.commentId = commentId;
        this.replyText = replyText;
        this.userId = userId;
        this.userName = userName;
        this.createTime = createTime;
        this.replyAvatar = replyAvatar;
    }

    public Reply() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public String getReplyText() {
        return replyText;
    }

    public void setReplyText(String replyText) {
        this.replyText = replyText;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getReplyAvatar() {
        return replyAvatar;
    }

    public void setReplyAvatar(String replyAvatar) {
        this.replyAvatar = replyAvatar;
    }

    @Override
    public String toString() {
        return "Reply{" +
                "id=" + id +
                ", commentId=" + commentId +
                ", replyText='" + replyText + '\'' +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                ", createTime=" + createTime +
                ", replyAvatar='" + replyAvatar + '\'' +
                '}';
    }
}