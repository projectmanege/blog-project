package com.blog.blogproject.bean;

import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * 粉丝关注实体类
 */
@Document(collection = "fans")
public class Fans implements Serializable {

    private Integer id;         //自动递增
    private Integer user_id;    //被关注用户id（非用户账号）
    private Integer fans_id;    //粉丝id（非用户账号）

    public Fans() {
        super();
    }

    public Fans(Integer id, Integer user_id, Integer fans_id) {
        this.id = id;
        this.user_id = user_id;
        this.fans_id = fans_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getFans_id() {
        return fans_id;
    }

    public void setFans_id(Integer fans_id) {
        this.fans_id = fans_id;
    }

    @Override
    public String toString() {
        return "Fans{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", fans_id=" + fans_id +
                '}';
    }
}
