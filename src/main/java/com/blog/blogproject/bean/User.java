package com.blog.blogproject.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.annotation.Id;
//import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;
import org.springframework.validation.annotation.Validated;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户实体类
 */
@Document(indexName = "user")
//@Document(collection = "user")
public class User implements Serializable {

    private Integer id;             //自动递增
    private Integer user_id;        //用户账号*
    private String user_name;       //用户名*
    private String password;        //密码*
    private String avatar;          //存放用户头像的路径
    private String user_sex;        //性别
    private Date create_time;       //账号创建时间*
    private Date birthday;          //生日
    private String phone_number;    //手机号码
    private String email;           //电子邮箱
    private String strBirthday;          //生日



    public User() {
        super();
    }

    public User(Integer id, Integer user_id, String user_name, String password, String avatar, String user_sex, Date create_time, Date birthday, String phone_number, String email, String strBirthday) {
        this.id = id;
        this.user_id = user_id;
        this.user_name = user_name;
        this.password = password;
        this.avatar = avatar;
        this.user_sex = user_sex;
        this.create_time = create_time;
        this.birthday = birthday;
        this.phone_number = phone_number;
        this.email = email;
        this.strBirthday = strBirthday;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUser_sex() {
        return user_sex;
    }

    public void setUser_sex(String user_sex) {
        this.user_sex = user_sex;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStrBirthday() {
        return strBirthday;
    }

    public void setStrBirthday(String strBirthday) {
        this.strBirthday = strBirthday;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", user_name='" + user_name + '\'' +
                ", password='" + password + '\'' +
                ", avatar='" + avatar + '\'' +
                ", user_sex='" + user_sex + '\'' +
                ", create_time=" + create_time +
                ", birthday=" + birthday +
                ", phone_number='" + phone_number + '\'' +
                ", email='" + email + '\'' +
                ", strBirthday='" + strBirthday + '\'' +
                '}';
    }
}
