package com.blog.blogproject.bean;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection = "comment")
public class Comment {
    private Integer id;

    private Date createTime;

    private String text;

    private Integer blogsId;

    private Integer userId;

    private Integer likes;

    private String avatar;

    private List<Reply> replyList;

    private String userName;

    public Comment(Integer id, Date createTime, String text, Integer blogsId, Integer userId, Integer likes, String avatar, List<Reply> replyList, String userName) {
        this.id = id;
        this.createTime = createTime;
        this.text = text;
        this.blogsId = blogsId;
        this.userId = userId;
        this.likes = likes;
        this.avatar = avatar;
        this.replyList = replyList;
        this.userName = userName;
    }

    public Comment() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getBlogsId() {
        return blogsId;
    }

    public void setBlogsId(Integer blogsId) {
        this.blogsId = blogsId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public List<Reply> getReplyList() {
        return replyList;
    }

    public void setReplyList(List<Reply> replyList) {
        this.replyList = replyList;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", createTime=" + createTime +
                ", text='" + text + '\'' +
                ", blogsId=" + blogsId +
                ", userId=" + userId +
                ", likes=" + likes +
                ", avatar='" + avatar + '\'' +
                ", replyList=" + replyList +
                ", userName='" + userName + '\'' +
                '}';
    }
}