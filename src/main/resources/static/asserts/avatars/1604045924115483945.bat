@echo off

:start

echo ============请输入数字选择下一步操作:

echo ============1：ip设置为192.168.62.119

echo ============2：ip设置为134.128.58.250

echo ============3：ip设置为自动获取

set /P var=":"

if %var%==1 goto ip83

if %var%==2 goto ip250

if %var%==3 goto ipdhcp

:ip83

cls

netsh int ip set addr "以太网" static 192.168.62.119 255.255.255.0 192.168.62.1
netsh int ip set dns "以太网" static 192.168.10.10 primary validate=no

echo **IP设置为192.168.62.119，设置成功**

echo ------------------------------------------

goto start

:ip250

cls

netsh int ip set addr "以太网" static 134.128.58.250 255.255.255.0 134.128.58.1
netsh int ip set dns "以太网" static 134.128.34.50 primary validate=no

echo **IP设置为134.128.58.250（内网IP），设置成功**

echo ------------------------------------------

goto start

:ipdhcp

cls

netsh interface ip set address name="以太网" source=dhcp

netsh interface ip delete dns "以太网" all

ipconfig /flushdns

echo **IP设置为DHCP获取，设置成功**

echo ------------------------------------------

goto start