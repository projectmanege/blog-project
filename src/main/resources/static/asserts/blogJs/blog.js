$('.menu.toggle').click(function () {
    $('.m-item').toggleClass('m-mobile-hide')
})

$("#publish_btn").click(function () {
    var comment = $("#comment").val();
    // alert(comment);
    $.ajax({
        url: "/blog/commentAdd",
        type: "POST",
        data: "comment=" + comment,
        success: function (result) {
            console.log(result);
            window.location.reload();
        }
    });
});

$(function () {
    $.ajax({
        url: "/blog/findCommentByUserAndBlog",
        data: {"userId": 1, "blogId": 1},
        type: "GET",
        success: function (result) {
            console.log(result);
        },
    })
});

